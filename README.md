<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [The SCutils package](#markdown-header-the-scutils-package)
- [Installing](#markdown-header-installing)
- [Common functions](#markdown-header-common-functions)
    - [General](#markdown-header-general)
    - [Gene name conversions](#markdown-header-gene-name-conversions)
    - [Genomedata](#markdown-header-genomedata)
    - [Gene coordinates and gene lists](#markdown-header-gene-coordinates-and-gene-lists)
    - [Looking up genes](#markdown-header-looking-up-genes)
- [Seurat convenience functions](#markdown-header-seurat-convenience-functions)
    - [inferCNV related:](#markdown-header-infercnv-related)
- [10X convenience functions](#markdown-header-10x-convenience-functions)
- [Sharq and SORTseq functions](#markdown-header-sharq-and-sortseq-functions)
    - [Well sets](#markdown-header-well-sets)
    - [Functions](#markdown-header-functions)
- [Maintainer notes](#markdown-header-maintainer-notes)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# The SCutils package

This package provides common utility functions, and functions that are
more specific to 10x and SORTseq/Sharq. In addition, genome data 
such as gene coordinates and genelists are made available by doing something like
`data(refdata_gex_mm10_2020_A)`. (Do `?genomedata` for full documentation)

# Installing

```
# The devtools library is probably already installed, but if not do so as:

> install.packages("devtools")

#  Next install the SCutils package as:

> devtools::install_bitbucket("princessmaximacenter/scutils")

# To start using it do

> library(SCutils)  # note: uppercase 'SC'

# To make use of the data (e.g. the genelists), do e.g. 

data(refdata_gex_GRCh38_2020_A) # do ?genomedata for documentation

```
# Common functions

## General
 * `stop.unless.unique, is.empty.string, is.nonempty.string` -- checking stuff
 * `commafy, decommafy, format.kMG, sortable_integer_strings` -- formatting numbers
 * `read.json, read.tab, write.tab`  -- reading/writing files
 * `dumper`  -- error handler to avoid loosing data upon crashes in long-running jobs
 * `hasGOid, GOterm.re.search, lookup.genes.byGOid, lookup.genes.byGOterm`  -- utility functions to work with GO.db and the "org.db"'s.
 * `.gitversion` -- get the exact git version of the package

## Gene name conversions

 * `unENSG, unSYMB, human2mouse_orthologues, mouse2human_orthologues, rename.genes` -- reformatting/renaming genes
 * when using the `genomedata` (see there), the lookup tables `gene_name2ensembl`
   and `ensembl2gene_name` are available, which is probably easier and faster.

## Genomedata

Genome data such as coordinates and predefined gene lists are available
by doing a `data(GENOMENAME)` statement with the genome of
interest. Currently defined are the datas sets corresponding to the 10x
genomics reference genomics:

 * refdata_gex_GRCh38_2020_A:        default 10X human
 * refdata_gex_mm10_2020_A:          default 10X mouse
 * refdata_cellranger_GRCh38_3.0.0:  obsolete 10x human

For more information do `?genomedata`

## Gene coordinates and gene lists
 
Gene coordinates and gene lists are available through a `data()`
statement, consisting of three objects: `genecoords`, a data.frame() with
genomic coordinates; `genelists`, a list() with pre-defined gene sets
that are commonly used, and lastly `genomedata`, a string vector with
version information for the previous two objects.

The contents of `genelists` are fixed per package version, and are provided
for convenience only. They  were obtained using the lookup functions
described below (see *Looking up genes*) and the `org.db` that is
described in the `genomedata` vector under `genomedata['orgdb_version']`.

Currently the coordinates come from our 10x reference genomes, whereas
genelists come from GO.db. This means that they may be slightly out of
date (GO.db releases use the Bioconductor release cycle). We are looking
into ways to improve this.  For full information do `?genomedata`.

To take full control over the genelist versions, use your own `org.db`
object and the lookup functions described below.

## Looking up genes

Is best done using the `genelists` list(), which (currently) has lists
for s.genes, g2m.genes, ribo, stress, hemo, female, male, chr6HLAgenes,
based on gene ontology and other sources. They are made available by
loading genomedata, see `?genomedata` for that.  In addition, the
following query functions are available:

 * `GOterm.re.search, lookup.genes.byGOid, lookup.genes.byGOterm`
 * `lookup.ribogenes, lookup.stressgenes, lookup.hemogenes, lookup.femalegenes, lookup.malegenes, lookup.cellcycle.genes`

# Seurat convenience functions

 * `saneAddModuleScore`,`AddLookedUpMetaData`,`DimredToMetadata` -- working with cell metadata
 * `metadataCorrelations`,`derivedCellcycleGenes` -- finding additional confounder genes
 * `loadings.MAD.plot`  -- judging principle components
 * AddDimReduc -- add additional (external) dimensional reduction coordinates

## inferCNV related:
 
 * `biomart_gene_coordinates`  -- Get gene coordinates using EnsEMBL's biomart
 * `coordinates_from_gtf` -- Read gene coordinates from a 10X Genomic style reference genome
 * `gene_order2GRanges` -- convert inferCNV gene_order to a GRanges object (needed by  `bulkCNV2infercnv`)
 * `bulkCNV2infercnv` -- Align bulk sequencing CNV calls with inferCNV calls
 * `aneuploidy_score` -- one score per cell indicating presence of CNV
 * `addCNVAssay` -- Create a new Seurat assay containing inferCNV's "Modified Gene Expression" values

# 10X convenience functions

 * `fast_merge_sparse` -- columnwise concatenation of sparse matrices (much faster than naive version)
 * `read10xlibs` -- reading several 10x libraries without hassle, either from `h5` files or `filtered_feature_bc_matrix` directories. Note: currently it can only read gene expression h5 files
 * `prepare.10x.annotation` -- Create data.frame with the essential gene annnotation

# Sharq and SORTseq functions 

## Well sets

Central to our SORTseq functions are so-called `wellset`s: sets of wells
in the plate. They are used to specify which wells are required, either
user-specified: e.g. which wells were empty (i.e. used for controls), or
conversely, data-implied, especially which wells are deemed to contain
live cells.  The most important function to work with this is
`find.wellsets()`, and the most important wellset it finds is
`wellset$fullgood` (see below).

## Functions
 * `read.counts` -- utility function to read count tables
 * `find.wellsets` -- general function to find all the wellsets
 * `wellname` -- return name or number of a 384-well plate well
 * `known.wellsets` -- return names of standard wellsets (such as col1 etc.)
 * `lookup.wellset` -- Lookup sets of wells by their shorthand name, and/or syntax check
 * `goodERCCs` -- find ERCCs that are good enough
 * `goodwells` -- find the wells were reactions are good
 * `livecellcutoff` -- determine a good threshold for which cells are likely to be live

An example of loading the count tables produced by SORTseq and Sharq is shown here:

```
    file <- '~/git/platediagnostics/test/testdata/test.transcripts.txt'
    count.table <- read.counts(file)
    wellsets <- find.wellsets(data=count.table, empties='col24', badERCCs = "use.top3")
    # (the badERCCs argument should not be needed but the test set is too bad/small)

    # Show overview of all the wellsets that were defined:
    cbind(unlist(lapply(wellsets, length)))

    # get rid of failed wells and of empty wells:
    count.table <- count.table[ , wellsets$fullgood ]
    
    # only keep nuclear genes:
    genes <- rmmito(rmspike(count.table))

```

# Maintainer notes

When releasing + installing a new version, be sure to do the following:
 
 * If not yet done, `git clone` this repository, e.g. into your `~/git/` directory, and `cd ~/git/SCutils`
 * There are hook scripts that update the `GIT\_VERSION` file for the `.gitversion()` function which has to be 'baked into' the package before installing it. Install the hooks as follows, *before installing the R package itself* :
    
        cd ~/git/SCutils # or wherever you git clone'd it
        cp -f git_hooks/post-commit .git/hooks/post-commit  # DONT SYMLINK
        ## check contents, then make executable:
        chmod a+x .git/hooks/post-commit
        # run it once to create the first GIT_VERSION file:
        .git/hooks/post-commit
        # install the hooks:
        cd .git/hooks
        for f in checkout merge rebase; do
          ln -fs post-commit post-$f
        done
        cd -
   
   The above code needs to be done only once for each git clone.
   It installs the hook under three different names so that
   it runs upon all the relevant git actions. Note that hooks can
   change unknowningly after e.g. a `git pull`. Since hooks run
   automatically this can therefore be dangerous. So instead of
   symlinking, double check the content and copy it as indicated above.
 
 * Install the latest version and reload the package into your session so that 
   any code used to create data  that is part of the package is also 
   the latest version.
 * If there is new data or a new way of producing it (see genelists.R
   and genecoords.R), run the code producing it (it should go to the
   data/ subdir) and git commit that new data too
 * If there is changed or new documentation, run roxygenize so that all
   `*.Rd` are up to date
 * Make sure the package can be installed by doing `R CMD INSTALL SCutils`
 * Add new functions to this README.md with a very short description
 * Update this README's Table of Contents by running `doctoc --bitbucket` (see
   https://github.com/thlorenz/doctoc ): `doctoc --bitbucket README.md`
 * Commit the changes in a private local branch (e.g. `philip`), preferably in commits not much smaller or larger than 1 bug.
 * Merge with master, as (here using private local branch `philip`):
   `git co master; git pull; git merge --no-ff philip`. After this, delete your branch
   Stay in the master branch.
 * Edit `DESCRIPTION`: change date and version
 * Edit `NEWS.md` to include the most important new bits of this new version
 * To be safe, check if package is still installable using `R CMD INSTALL SCutils`
 * git commit DESCRIPTION and NEWS.md with a line like `version bump to v104` (to master branch)
 * add a new tag (here: `v104`) and push it to bitbucket
 * At the HPC, do `cd ~/git/SCutils ; git pull` to update your own working copy
 * Lastly install it, e.g. as 
  
        R CMD INSTALL -l /hpc/local/CentOS7/pmc_research/R_libs/4.1.2 '.'
 
 * announce it to the JOIN scgenomics channel
