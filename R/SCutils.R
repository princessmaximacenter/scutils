#' Find out the commit of the current package
#'
#' This relies on the presence of a file called GIT_VERSION (itself
#' unversioned), produced by the post-commit and post-checkout hooks
#' that were copied from PKG/git_hooksd to
#' PKG/.git/hooks/post-{commit,checkout}. Note that this is independent
#' of the R package version found in DESCRIPTION.
#' @return A string like "SCutils: philip, v96-2-812c824, Thu Feb 17 09:42:22 2022 +0100"
#' @keywords misc
#' @author Philip Lijnzaad <plijnzaad@gmail.com>
.gitversion <- function() {
  pkg <- getPackageName()
  if(is.null(pkg))
    return("NOT A PACKAGE")
  f <- paste0(path.package(pkg), "/GIT_VERSION")
  if( file.exists(f) )
    v <- readLines(f)
  else
    v <- "UNKNOWN GIT COMMIT"
  paste0(pkg, ": ", v)
}

### internal functions start with a '.', may not have a man page and may not be exported

## miscellaneous small functions

#' chop the ENSG-bit  off sortseq gene identifiers
#'
#' @param v A vector of gene ids
#' @examples
#' unENSG("ENSG000001234567--FOOBAR1")
#' unENSG("ENSMUSG001234567__Foobar1")
#' @return  A vector of gene ids
unENSG <- function(v)gsub("^ENSG(MUS)?\\d+[-_][-_]*", "", v, perl=TRUE)

#' chop the ENSG-bit  off sortseq gene identifiers
#'
#' @param v A vector of gene ids
#' @examples
#' unSYMB("ENSG000001234567--FOOBAR1")
#' unSYMB("ENSMUSG001234567__Foobar1")
#' @return  A vector of gene ids
unSYMB <- function(v)gsub("[-_].*$", "", v, perl=TRUE)

#' short-hand function for checking uniqueness
#'
#' @param thing A vector or matrix
#' @examples
#' stop.unless.unique(sample(letters, size=6,replace=TRUE))
#' @return  Nothing
stop.unless.unique <- function(thing) {
  stopifnot(is.atomic(thing))
  dim(thing) <- NULL
  stopifnot(sum(duplicated(thing))==0)
}

#' short-hand boolean function for checking string emptiness
#'
#' @param s A string or vector of strings
#' @examples
#' is.empty.string(sample(c("", "a", "b")))
#' @return  TRUE or FALSE
#' @seealso is.nonempty.string
is.empty.string <- function(s)
  sapply(X=s, FUN=function(ss){is.null(ss)||length(ss)==0||ss==""})

#' describeIn is.empty.string  Oppososite of is.empty.string
#' @seealso is.empty.string
is.nonempty.string <- Negate(is.empty.string)

#' Convenience function for renaming genes
#'
#' @param genes  A vector of gene names
#' @param lookup  A named vector to find the new names
#' @return  A vector of gene names
#' @examples
#' rename.genes(letters, c(a='A', c='C', j=NA))
rename.genes <- function(genes, lookup) {
  ## e.g. rename.genes(genes=c('ASDF', 'FOO', 'QWER', 'XYZ'), lookup=c(XYZ='PQR',  FOO='BAR'))
  stopifnot(!is.null(names(lookup)) && sum(names(lookup)=="") == 0 )
  to.rename <- genes %in% names(lookup)
  genes [ to.rename ]  <- unname(lookup[ genes[to.rename] ])
  return(genes)
}

#' Put thousand-separators into numbers
#'
#' Make big numbers more readable using ',' as separator of thousands
#' @param x vector of numerics
#' @param preserve.width wether to produces strings of equal length (see
#'   \code{\link{formatC}}). The defaul is to left-pad all numbers to
#'   equal length
#' @return  A string with commas with commas between the powers of 1000
#' @examples
#' commafy(round(2^sort(runif(10,min=3,max=31))))
#' @seealso \code{\link{decommafy}},\code{\link{format.kMG}},
#'   \code{\link{formatC}}
#' @author Philip Lijnzaad <plijnzaad@gmail.com>
#' @note taken from uuutils, https://bitbucket.org/princessmaximacenter/
#'   uuutils.git v1.55 2021-01-07 13:52
commafy <- function(x, preserve.width="common") { 
## for clever recursive solution, see prior to rev.1.288 
    n <- names(x)
    res <- formatC(x, format="d", big.mark=",", preserve.width=preserve.width)
    names(res) <- n
    res
}

#' Get rid of thousand-separators in character strings
#'
#' Opposite of \code{\link{commafy}}. If the input contains non-numeric
#'   strings they will become \code{NA}.
#' @param x strings to decommafy
#' @return numeric vector (not integer)
#' @examples withcommas <-
#'   commafy(round(2^sort(runif(10,min=3,max=31))))
#'   decommafy(withcommas)
#' @seealso \code{\link{decommafy}},\code{\link{format.kMG}},
#'   \code{\link{formatC}}
#' @author Philip Lijnzaad <plijnzaad@gmail.com>
#' @note taken from uuutils,
#'   https://bitbucket.org/princessmaximacenter/uuutils.git v1.55
#'   2021-01-07 13:52
decommafy <- function(x){
    n <- names(x)
    res <- as.numeric(gsub(",", "",x))
    dim(res) <- dim(x)
    names(res) <- n
    res
}

#' Make integers used as strings sort numerically properly
#'
#' Occasionally integers are converted to strings, after which they sort as
#' "1", "10", "11" instead of "1", "2", etc. This function turns a vector such as
#' c("1", "10", "100", "1000") into c("0001", "0100", "1000"). All 'integer strings'
#' will be of the same length as long as longest number.
#' @param x    Vector of integers or strings to be turned into sortable integer strings
#' @return A string vector that sorts numerically
#' @note For non-integer numbers this function will raise an error
sortable_integer_strings <- function(x) {
  x <- as.character(x) # may not yet be string
  if(any(grepl("[-.e]", x)))
    stop("sortable_integer_strings: argument not positive integer-like")
  fmt <- sprintf("%%0%dd", max(nchar(x))) # yields e.g. "%02d"
  sprintf(fmt,as.integer(x))           # yields e.g. "00",  ... , "23"
}

#' Read JSON from a (section of) a file
#'
#' @param file Which file or connection to read from (passed to
#'   \code{\link{readLines}})
#' @param start.regexp,end.regexp Markers in the file that indicate the
#'   start and end of the JSON section in the file. The markers are
#'   interpreted as case insensitive perl regeps.  If NULL, the complete
#'   file is assumed to be valid JSON
#' @param mustWork If true, parsing failures or absence of JSON section
#'   is fatal (otherwise just warn)
#' @return a list.
#' @author Philip Lijnzaad <plijnzaad@gmail.com>
#' @note taken from uuutils,
#'   https://bitbucket.org/princessmaximacenter/uuutils.git v1.55
#'   2021-01-07 13:52
read.json <- function(file,
                      start.regexp="^#+ *[-=]* *json +start",
                      end.regexp="^#+ *[-=]* *json +end",
                      mustWork=TRUE) { 
    library(jsonlite)
    stopifnot( is.null(start.regexp) == is.null(end.regexp)) #either both NULL or both character
    stopifnot( is.character(start.regexp)  == is.character(end.regexp))
    oops <- if(mustWork) stop else function(...){warning(...); return(NULL) }

    lines <- readLines(file)

    start <- end <- NULL
    if(! is.null(start.regexp) ) { 
        check.re <- function(re,lines) {
            found <- grep(re, lines, perl=TRUE, ignore.case=TRUE)
            len <- length(found)
            if(len != 1) {
                oops(sprintf("Expected exactly one pattern '%s' in file %s, found %d", re, file,len))
            }
            found
        }
        start <- check.re(start.regexp, lines)
        if(length(start)==0)
          return(oops("Did not find start of JSON block\n(",start.regexp," missing)\n"))
        end <- check.re(end.regexp, lines)
        if(length(end)==0)
          return(oops("Did not find end of JSON block\n(",end.regexp," missing)\n"))
    } else {
        start <- 1
        end <- length(lines)
    }
    len <- end - start -1
    if(len <= 0)
      oops("Not enough JSON lines in file ", file, " (or start and end reversed)")
    json <- paste(lines[(start+1):(end-1)], collapse="", paste="")
    res <- NULL
    try(res <- fromJSON(txt=json, simplifyVector=TRUE))
    if(is.null(res))
      oops("Error parsing the following JSON\n\n", json, "\n\n,read from file ", file)
    res
}                                       #read.json

#' Format numbers using metric prefixes (k for 1000, M for 1e6, G for 1e9, etc)
#' 
#' Arguments must be > 1e-18 and < 1e24. The prefix for 1e-6 (micro)
#'     should be be the greek symbol mu, but 'u' is used instead so as
#'     not to have to use UTF encodings
#' @param x Vector of numbers to format
#' @param fmt The format to use for the part before the prefix (the
#'   magnitude of which has changed).  It must start with a number
#'   format (typically %f with some precision) and the string format
#'   used for the prefix.
#' @return a character vector
#' @examples format.kMG(10^sort(rcauchy(10))) # (this may over- or underrun, try again if needed)
#' @seealso \code{\link{commafy}}, \code{\link{sprintf}}, \code{\link{formatC}}, \code{\link{prettyNum}}
#' @note taken from uuutils, https://bitbucket.org/princessmaximacenter/uuutils.git v1.55 2021-01-07 13:52
format.kMG <- function(x, fmt="%.0f%s") {
  ## use kilo, mega, giga etc. prefixes to print numbers (and likewise for numbers < 1)
  ## first format specifier does the number, second is the prefix
  stopifnot(all(1e-18 < x  & x < 1e+24))
  prefixes <- c("a", "f", "p", "n", "u","m", "", "k","M", "G", "T", "P", "E", "Z", "Y")
  offset <- which( prefixes=="" )
  f <- floor(floor(log10(x))/3)
  prefix <- prefixes[f+offset]
  om <- 10^(3*floor(floor(log10(x))/3))
  scaled <- x/om
  sprintf(fmt, scaled, prefix) 
}                                       #format.kMG

#' Read tab-delimited table ...
#'
#' ... in a standard way. First row is column names, First column is
#' rownames, topleft cell is empty. Reads tables produced by
#' \code{\link{write.tab}}.
#' @param file File name to read from.
#' @param ...  Further stuff is passed to \code{\link{read.table}}
#' @return data.frame
#' @keywords misc
#' @seealso write.tab
#' @author Philip Lijnzaad <plijnzaad@gmail.com>
#' @note taken from uuutils,
#'   https://bitbucket.org/princessmaximacenter/uuutils.git v1.55
#'   2021-01-07 13:52
read.tab <- function(file, ...)
  read.table(file=file, sep="\t", as.is=T, quote="", header=T, comment.char="", row.names=1, ...)

#' Write tab-delimited table  ...
#'
#' ... in a standard way. Use row- and column names must be defined;
#' topleft cell remains empty. Can be read by \code{\link{read.tab}}.
#' @param x Data frame with rownames and column names.
#' @param file File name to write to.
#' @param ...  Further stuff is passed to \code{\link{write.table}}
#' @return Nothing
#' @keywords misc
#' @seealso read.tab
#' @author Philip Lijnzaad <plijnzaad@gmail.com>
#' @note taken from uuutils,
#'   https://bitbucket.org/princessmaximacenter/uuutils.git v1.55
#'   2021-01-07 13:52
write.tab <- function(x, file="", ...)
  write.table(x, file, sep="\t", quote=F, row.names=T, col.names=NA, na="", ...)

#' dumper
#'
#' When debugging, or when running very long-lasting jobs, it can be
#' useful to save coredumps.  This function is an error handler that
#' does that. Upon error (or warning) it will dump a stack trace and/or
#' memory dump for post-mortem debugging and/or data salvager.
#' @param dir Where to dump to; default: current directory
#' @param what What to dump: "stack+data", "stackonly" or "dataonly"
#' @param suffix What to suffix the dumps with (typically a scriptname
#'   and/or pid)
#' @note When dumping to a different directory, the name of the dump
#'   object will contain slashes, so accessing it (after e.g. \code{
#'   \link{ load("/var/tmp/framedump_myscript27744.rda") } } ) will
#'   require backticks (e.g. get("my.data",
#'   env=`/var/tmp/framedump_myscript27744`[[5]])
#' @note To force dumps even for warnings, do \code{option(warn=2)}
#' @seealso \code{\link{dump.frames}}, \code{\link{debugger}}
#' @examples \dontrun{
#' ## Installing the handler:
#' options(error=dumper()) # simple case
#' options(error=dumper(dir="/var/tmp", dump="stackonly", suffix=sprintf("myscript_%s_%d", Sys.getenv("USER"), Sys.getpid())))
#' ## Uninstalling the handler:
#' options(error=NULL)
#' }
#' 1 # create example-section
#' @author Philip Lijnzaad <plijnzaad@gmail.com>
dumper <- function(dir=".",
                   what="stack+data", 
                   suffix="") {
  ## Error handler, install  with:
  ##
  ##   # options(warn=2) # <- to get dumps even for warnings() !
  ##   options(error=dumper(), show.error.messages=T)
  ##
  ## (uninstall with options(error=NULL) ).
  ##
  options(show.error.messages=TRUE)
  choices <- c("stack+data","stackonly", "dataonly")
  dump <- pmatch(what, choices )
  if(is.na(dump))
    stop("Invalid dump method ", what)
  dump <- choices[dump]
  dump.stack <- grepl("stack", dump)
  dump.data <- grepl("data", dump)
  if(suffix!="")
    suffix <- paste0("_", suffix)
  return(function() {
    options(error=NULL)   #otherwise infinite recursion if dump fails
    options(width=200)    #work-around the dump.frames / limitLabels bug

    if(dump.stack) {
      file <- sprintf("%s/framedump%s", dir, suffix) # automatically gets '.rda' ext
      file <- sub("^\\./+","", file) # dump object named `./something.rda` would be invisible!
      message("dumping frames to ", file, ".rda\n",
              call. = TRUE, immediate. = TRUE)
      dump.frames(dumpto=file, to.file=TRUE)
      cat("done dumping\n")
    }
    
    if(dump.data) { 
      file <- sprintf("%s/datadump%s.rda",dir, suffix) # does *NOT* automatically get '.rda' ext
      message("dumping data to ", file, "\n", 
              call. = TRUE, immediate. = TRUE)
      save.image(file=file)
    }
    message("Exiting now\n", call. = TRUE, immediate. = TRUE)
    quit(save="no", status=1)
  })                                    #function
}                                       # dumper

#' human2mouse_orthologues
#'
#' Simple conversion from human to mouse gene id's based on orthologue
#' mapping as provided by EnsEMBL's biomart. 
#' @param hgcn_ids Human gene ids
#' @param host Host to use. The default
#'   corresponds to the 10X refdata-gex-GRCh38-2020-A human genome,
#'   which is gencode 32 (ensemble 98), assembly GRch38.p13
#' @author based on https://github.com/satijalab/seurat/issues/2493 and
#'   from
#'   https://www.r-bloggers.com/2016/10/converting-mouse-to-human-gene-names-with-biomart-package/
#' @return A named vector of MGI gene symbols. The names are the HGCN
#'   symbols, so you can directly look things up.  Note that resulting
#'   MGI symbols need not be unique; their human names *are* unique,
#'   otherwise you can't use it as a lookup.  Duplicates in MGI symbols
#'   as well as human id's that couldn't be found are warned about.
#' @seealso genelists, mouse2human_orthologues
#' @note For the AnnotationDbi + Orthology.eg.db route see e.g.
#'   https://support.bioconductor.org/p/9143874/ 
human2mouse_orthologues <- function(hgcn_ids,
                                    host='https://sep2019.archive.ensembl.org') {
  library("biomaRt")

  hgcn_ids <- unique(hgcn_ids)
  if(any(duplicated(hgcn_ids)))
    warning("Ignoring duplicates in hgcn_ids")
  
  human = useMart("ENSEMBL_MART_ENSEMBL",
                  dataset = "hsapiens_gene_ensembl",
                  host = host)
  
  mouse  <-  useMart("ENSEMBL_MART_ENSEMBL", # NOT "ENSEMBL_MART_MOUSE" !
                     dataset = "mmusculus_gene_ensembl",
                     host = host)

  df  <-  getLDS(mart = human,
                 martL = mouse,
                 attributes = c("hgnc_symbol"),
                 filters = "hgnc_symbol",
                 attributesL = c("mgi_symbol"),
                 values = hgcn_ids, 
                 uniqueRows=TRUE)
  
  h.genes <- df[[1]]
  m.genes <- df[[2]]
  names(m.genes) <- h.genes
  
  missing <- setdiff(hgcn_ids, h.genes)
  if(length(missing)>0)
    warning("**** Could not find mouse orthologues for ", paste(collapse="\n", missing), "\n **** \n")

  dups <- duplicated(h.genes)
  if(sum(dups)>0)
    warning("**** Non-unique human orthologues for following mouse genes ", paste(collapse="\n", unique(m.genes[dups])), "\n **** \n")    
  return(m.genes)
} # human2mouse_orthologues

#' mouse2human_orthologues
#'
#' Simple conversion from mouse to human gene id's based on orthologue
#' mapping as provided by EnsEMBL's biomart
#' @param mgi_ids Mouse gene ids
#' @param host Host to use. The default
#'   corresponds to the 10X refdata-gex-mm10-2020-A mouse genome,
#'   which is gencode M23 (ensemble 98), assembly GRCm38.p6
#' @author based on https://github.com/satijalab/seurat/issues/2493 and
#'   from
#'   https://www.r-bloggers.com/2016/10/converting-human-to-mouse-gene-names-with-biomart-package/
#' @return A named vector of HGCN gene symbols. The names are the MGI
#'   symbols, so you can directly look things up.  Note that the
#'   resulting HGCN symbols need not be unique; their mouse names *are*
#'   unique. Duplicates in HGNC
#'   symbols as well as mouse id's that couldn't be found are warned
#'   about.
#' @seealso genelists, human2mouse_orthologues
#' @note For the AnnotationDbi + Orthology.eg.db route see e.g.
#'   https://support.bioconductor.org/p/9143874/ 
mouse2human_orthologues <- function(mgi_ids,
                                    host='https://sep2019.archive.ensembl.org') {
  library("biomaRt")
  if(any(duplicated(mgi_ids)))
    warning("Ignoring duplicates in mgi_ids")
  mgi_ids <- unique(mgi_ids)
  
  human = useMart("ENSEMBL_MART_ENSEMBL",
                  dataset = "hsapiens_gene_ensembl",
                  host = host)
  
  mouse  <-  useMart("ENSEMBL_MART_ENSEMBL", # NOT "ENSEMBL_MART_HUMAN" !
                     dataset = "mmusculus_gene_ensembl",
                     host = host)

  df  <-  getLDS(mart = mouse,
                 martL = human,
                 attributes = c("mgi_symbol"),
                 filters = "mgi_symbol",
                 attributesL = c("hgnc_symbol"),
                 values = mgi_ids, 
                 uniqueRows=TRUE)
  
  m.genes <- df[[1]]
  h.genes <- df[[2]]
  names(h.genes) <- m.genes
  
  missing <- setdiff(mgi_ids, m.genes)
  if(length(missing)>0)
    warning("**** Could not find human orthologues for ", paste(collapse="\n", missing), "\n **** \n")

  dups <- duplicated(h.genes)
  if(sum(dups)>0)
    warning("**** Non-unique human orthologues for following mouse genes ", paste(collapse="\n", unique(h.genes[dups])), "\n **** \n")    
  return(h.genes)
} # mouse2human_orthologues

#' .fix_modulescore_columns
#'
#' Seurat (3?) adds irritating numbers when calling module scores. This gets
#' rid of them.
#' @param  obj  Seurat object
#' @param  names  char vector of names that may have hadd pointless digits added
#' @return Seurat object with saner meta.data column names
#' @seealso  \code{\link{saneAddModuleScore}}
.fix_modulescore_columns <- function(obj, orig.names) { 
  cols <- colnames(obj@meta.data)
  for(name in orig.names) {
    re <- paste0('^(' , name , ')_tfsug0p5_\\d+$')
    if(length(grep(re, cols)) !=1) {
      stop()
    }
    cols <- gsub(re , "\\1", cols, perl=TRUE)
  }
  stop.unless.unique(cols)
  colnames(obj@meta.data) <- cols
  return(obj)
} # .fix_modulescore_columns

#' saneAddModuleScore
#'
#' AddModuleScore adds irritating sequential numbers to the column name
#' of added module scores; this function avoids that. If a new module
#' score with the same name as an existing one is added, the old one is
#' overwritten.  In addition, - the 'RNA' assay is always used - a more
#' natural calling sequence is allowed, in which the names of the list
#' elements become the meta.data column names.  - empty gene lists are
#' valid and are warned about instead of crashing - empty genelist can
#' be ignored, or result in using substituted values.
#' @param obj as for AddModuleScore
#' @param features as for AddModuleScore, but using the names of the
#'   list elements as the column name for the resulting module
#' @param names as for AddModuleScore, but best left unused.
#' @param empty Substitute value to use for empty genelists. If it is
#'   NULL, no module score is generated. If it is a single number, that
#'   is used. If it's a vector of length 2, a random number between
#'   min=empty[1],max=empty[2] (uniform distribution) is used for each.
#' @param ...  as for AddModuleScore.
#' @examples
#' \dontrun{
#' ## add columns 'erythro' and 'female' with the ModuleScore of the listed genes) :
#' obj <- saneAddModuleScores(obj, list(erythro=c("HBB", "HBD", "HBG2", "HBM", "HBA2", "HBA1"),
#'    female=c("XIST", "TSIX"))
#' }
saneAddModuleScore <- function(obj, features, names=NULL,
                               empty=NULL, ...) {
  assertList(features, all.missing=FALSE)
  assertNamed(features, type='unique')

  empties <- names(which(unlist(lapply(features, length))==0))
  
  if(length(empties)>0) {
    warning("saneAddModuleScore: following genelists are empty: ",
            paste(collapse=" ", empties), "\n")
     features <- features[ which(unlist(lapply(features, length)) > 0)]
  }
  
  if(is.null(names)) { 
    names <- names(features)
  } else { 
    if(!is.null(names(features)))
      stop("saneAddModuleScore: features has names but names are also provided, confusion!")
  }  

  ## AddModuleScore silently renames module scores to appease R, need to
  ## do that here as well
  names(features) <- gsub("[ :-]", ".", names(features))
  names <- gsub("[ :-]", ".", names)

  ## add separator so the added '1,2,3' don't interfere with numerical
  ## modulescore names
  orig.names <- names
  names <- gsub("$", '_tfsug0p5_', names)
  names(features) <- gsub("$", '_tfsug0p5_', names(features))
  
  save.assay <- DefaultAssay(obj)
  DefaultAssay(obj) <- 'RNA'
  
  for(name in orig.names) {
    if(!is.null(obj@meta.data[[name]])) {
      warning("Overriding column ", name)
      obj@meta.data[[name]] <- NULL
    }
  }
  obj <- AddModuleScore(obj=obj, feature=features, name=names, ...)

  ## deal with empties:
  if(is.null(empty)) { 
    warning("saneAddModuleScore: empty genelists are ignored")
  } else {
    expect_numeric(empty, min.len=1, max.len=2)
    if ( length(empty)==1 ) {
      warning("saneAddModuleScore: empty genelists get module score ", empty)
      substitute <- rep(empty, ncol(obj))
    } else {
      warning("saneAddModuleScore: empty genelists get random module score between ", empty[1], " and ", empty[2])
      substitute <- runif(ncol(obj), min=empty[1], max=empty[2])
    }
    
    for (name in empties) {
      if(!is.null(obj@meta.data[[name]]))
        warning("Overriding column ", name)
      obj@meta.data[[ name ]] <- substitute
    }
  }
  
  DefaultAssay(obj) <- save.assay 
  return( .fix_modulescore_columns(obj=obj, orig.names=orig.names))
} # saneAddModuleScore

#' metadataCorrelations
#'
#' calculate the correlations of genes with cell metadata over all
#' cells. This is sometimes done to find additional genes that correlate
#' with a biological process: removing such correlating genes from the
#' list of variable genes can help deconfounding.
#' @param srat Seurat object
#' @param assay.type Which assay to use (this may be different from the
#'   default)
#' @param slot Which slot to use.
#' @param method Which correlation method to use (passed to function
#'   \code{\link{cor}}).
#' @param var Name of metadata column to use; must be a numeric
#'   column. A typical example are the `S.Score` or 'G2M.score` columns 
#'   that resulted from an earlier call to `CellCycleScoring`
#' @return named vector of correlations per gene; NA's have been
#'   replaced with 0.
#' @note SCTransform v2 (more precisely: run with `vst.flavor='v2'`) sets
#'   the expression of some lowly expressed genes to zero, leading to
#'   non-existant corelations (which are then set to 0). This is warned about.
#' @seealso `cor`; Seurat::AddMetaData; SCutils::saneAddModuleScore;
#'   SCutils::derivedCellcycleGenes2
#' @examples
#' \dontrun{
#' ## find genes correlating strongly with hemoglobin expression:
#' srat <- saneAddModuleScore(srat, features=genelists['hemo'])
#' hemoCor <- metadataCorrelations(srat, 'hemo')
#' q1 <- quantile(probs=0.25, x=hemoCor[genelists$hemo])
#' hemoCor <- sort(setdiff( names(which(hemoCor > q1)), genelists$hemo))
#' }
metadataCorrelations <- function(srat,
                                 var,
                                 assay.type=DefaultAssay(srat),
                                 slot='data',
                                 method='pearson') {
  v <- srat@meta.data[[var]]
  if(is.null(v) || !is.numeric(v))
    stop("Variable ", var, " not found in cell metadata, or not numeric")
  var <- v
  data <- slot( srat@assays[[assay.type]], slot)
  
  nzero <- sum(Matrix::rowSums(data)==0)
  if(nzero > 0)
    warning("metadataCorrelations: the expression of ", nzero, " genes was set to zero, \
probably by SCTransform using vst.flavor='v2'. \
Their correlation to the meta.data  will therefore also be zero")
  
  cor <- cor(var, t(as.matrix(data)), method=method)
  stopifnot(nrow(cor)==1)
  dim(cor) <- NULL 
  names(cor) <- rownames(data)
  cor[is.na(cor)] <- 0
  
  return(cor)
} # metadataCorrelations

#' derivedCellcycleGenes2
#'
#' Find and plot additional genes that correlate with cellcycle genes.
#' This is done to achieve better deconfounding. This is a newer, more
#' robust version of \code{\link{derivedCellcycleGenes}}.  The algorithm
#' works by performing a robust linear fit (using `MASS::rlm`) of the
#' correlations against the S-score, versus the correlations against the
#' G2M score. The genes that are in the top and bottom `quantile` of
#' *both* the correlation *and* the Pearson residual of the fit are
#' considered to be 'derived' cell cycle genes. The number of genes thus
#' found is limited to the `top_n` genes having the largest residual
#' (posititive for S genes, negative for G2M genes).
#' @param Scor,G2Mcor Named vectors of correlations per gene, over
#'   cells, to S.Score and G2M.Score (typically returned from
#'   `metadataCorrelations`). Vectors must be named by gene name.
#' @param Sgenes,G2Mgenes Genes constituting the S- and G2M-specific
#'   genes (best use the same ones as used with
#'   `Seurat::CellcycleScoring`).
#' @param quantile Genes having a correlation more extreme than this
#'   quantile of both the correlation and Pearson residual of the 'own
#'   group' are selected, then limited to be not more than the `top_n`
#'   genes having the most extreme residual
#' @param top_n The genes selected by the `quantile` parameter limited
#'   to be not more than the `top_n` genes having the largest residual
#' @return A \code{list(S.derived, G2M.derived, scatterplot, boxplot)}
#'   of additional genes, as well two ggplot2 objects containing a
#'   scatter plot of the correlations and boxplots of the correlations
#' @note Non-cellcycle related genes are transparent, but Seurat's
#'   S.Score and G2M.Score (calculated by \code{AddModuleScore})
#'   occasionally come out identical for certain genes. These will
#'   therefore 'stack' and appear as one dark dot.
#' 
#' @seealso \code{\link{derivedCellcycleGenes}, \link{metadataCorrelations}, \link{genomedata}}
#' @author Thanasis Margaritis, Philip Lijnzaad
#' @examples
#' \dontrun{
#' ## find genes correlating strongly with S.score and G2M.score
#' Scor <- metadataCorrelations(srat, 'S.Score')
#' G2Mcor <- metadataCorrelations(srat, 'G2M.Score')
#' derived <- derivedCellcycleGenes(Scor=Scor,
#'                                  Sgenes=genelists$s.genes,
#'                                  G2Mcor=G2Mcor,
#'                                  G2Mgenes=genelists$g2m.genes)
#' ## draw the plots:
#' show(derived$scatterplot | derived$boxplot )
#' ## add the newly found genes for general use:
#' genelists <- c(genelists, derived[ c("S.derived", "G2M.derived")])
#'
#' ## exclude the newly found genes from the VariableFeatures:
#' VariableFeatures(srat, assay='SCT') <- 
#'     setdiff( VariableFeatures(srat, assay='SCT'),
#'        unlist(derived[ c("S.derived", "G2M.derived")]))
#' }
derivedCellcycleGenes2 <- function(Scor, Sgenes, G2Mcor, G2Mgenes,
                                   quantile=0.25, top_n=100) {
  ## clean the lists
  allgenes <- names(Scor)
  Sgenes <- intersect(Sgenes, allgenes)
  G2Mgenes <- intersect(G2Mgenes, allgenes)

  library(MASS)
  
  # Convert the correlation data to a data frame and add a column for gene type
  df <- data.frame(Scor, G2Mcor)
  df$gene <- rownames(df)
  df$gene_type <- factor('other', levels=c("other", "known S", "known G2M",
                                           "S correlating", "G2M correlating")) # newly found
  df[ Sgenes, 'gene_type'] <- "known S"
  df[ G2Mgenes, 'gene_type'] <- "known G2M"
  
  rlm_cc <- rlm(Scor ~ G2Mcor) # robust linear fit

  ## Percentiles of both correlations to S and G2M and of the residuals:
  df$Scor_res <- rlm_cc$residuals
  df %>% filter(gene_type =="known S") %>% 
    summarize(q25_cor = quantile(Scor, probs = quantile), 
              q25_res = quantile(Scor_res, probs = quantile)) -> q25_s
  df %>% filter(gene_type =="known G2M") %>% 
    summarize(q25_cor = quantile(G2Mcor, probs = quantile), 
              q25_res = quantile(Scor_res, probs = (1-quantile))) -> q25_g2m
  
  ## Find genes in top quantiles of both correlation and residual, then take at most
  ## those in the top_n residuals:
  
  df %>% filter(Scor > q25_s$q25_cor & Scor_res > q25_s$q25_res) %>% 
    group_by(gene_type) %>% top_n(top_n, Scor_res) -> top_s
  
  df %>% filter(G2Mcor > q25_g2m$q25_cor & Scor_res < q25_g2m$q25_res) %>%
    group_by(gene_type) %>% top_n( -top_n, Scor_res) -> top_g2m

  known_types <- c("known S", "known G2M")

  Snew <- top_s[ !(top_s$gene_type %in% known_types), ]$gene
  df[ Snew, 'gene_type' ] <- "S correlating"
  
  G2Mnew <- top_g2m[ !(top_g2m$gene_type %in% known_types),  ]$gene
  df[ G2Mnew, 'gene_type' ] <- "G2M correlating"
  
  # Plot the new derived cell cycle genes 
  llim <- c(-0.5, 1)

  colors <- c(other='black', `known S`='lightpink', `known G2M`='lightblue',
              `S correlating`='red', `G2M correlating`='blue')
  
  alphas <- c(other=0.2, `known S`=1, `known G2M`=1,
              `S correlating`=1, `G2M correlating`=1)
  
  sizes <- c(other=0.2, `known S` = 1, `known G2M`=1,
             `S correlating`=1, `G2M correlating`=1)
  
  scatterplot <-
    ggplot(data = df,
           aes(x = G2Mcor, y = Scor, 
               color = gene_type,
               size = gene_type, 
               alpha = gene_type)) +
    coord_fixed(xlim=llim, ylim=llim, expand=FALSE) +
    scale_color_manual(values=colors) +
    scale_size_manual(values=sizes) +
    scale_alpha_manual(values=alphas) +
    geom_point() +
    ## geom_abline(slope = 1, intercept = 0, color = "black", linetype = "dashed")  +  # y=x line
    geom_abline(slope = rlm_cc$coefficients[2],
                intercept = rlm_cc$coefficients[1], color = "red", linetype = "dashed") +
    theme_bw()
  
  boxplot <-
    ggplot(data = df,
           aes(x = gene_type, y = Scor_res, color = gene_type,)) +
    geom_boxplot() + ylab("distance to regression line") + theme_bw() +
    scale_y_continuous(position = "right") +
    scale_color_manual(values=colors) +
    theme(axis.text.x=element_text(angle= -45, size=8, hjust=0),
          axis.text.y=element_text(size=10),
          axis.ticks=element_blank(),
          axis.title.x=element_blank(),
          legend.position="none") 
  
  return(list(S.derived=Snew, G2M.derived=G2Mnew,
              scatterplot=scatterplot, boxplot=boxplot))
} # derivedCellcycleGenes2


#' loadings.MAD.plot
#'
#' Per principle component, plot the number of genes having loadings
#' exceeding the median loadings of that PC by a certain number (f) of
#' MADs. This plot can be used to help determine the meaningful number of
#' PCs. The number of genes exceeding f*MAD tends to goes down on average;
#' once it flattens out you may be entering the realm of noise.
#' @param obj  Seurat object
#' @param MAD.factors the multiplication factors to try
#' @param win window size for smoothing out the resulting graphs
#' @return nothing
#' @seealso \code{\link{mad}}
loadings.MAD.plot <- function(obj, MAD.factors=seq(2,4), win=21) {
  ## number of genes per PC whose loadings exceed various
  ## MADs. Also draw a rolling median over WIN for each curve 'to guide
  ## the eye'.  Based on code by Thanasis but without the pos and neg
  ## (since they are arbitrary and largely the same)
  library(zoo)
  restore.mar <- par()$mar
  par(mar=c(4,4,4,4))
  
  PCdata <- obj@reductions$pca@feature.loadings
  meds <- apply(PCdata, 2, median)
  mads <- apply(PCdata, 2, mad)

  exceeding <- sapply(MAD.factors,
                      function(f)rowSums( abs((t(PCdata) -  meds)/mads) > f))
  
  colnames(exceeding) <- paste0('f=', MAD.factors)

  colors <- 1:ncol(exceeding)
  
  matplot(type='l',
          x=1:ncol(PCdata),
          y=exceeding,
          lty=1,
          col=colors,
          main='per PC, #genes whose loadings exceed f*MAD from median',
          xlab='PC', ylab='number of genes')
  legend(x='topright', legend=colnames(exceeding), pch=NULL, col=colors, lty=1)

  ## add smoothed line
  kk <- (win-1)/2
  roll <- apply(exceeding,2,rollmedian,k=2*kk+1)
  
  matlines(x=(1+kk):(ncol(PCdata)-kk),
           y=roll,
           lty=2,
           col=colors)
  par(mar=restore.mar)
} #loadings.MAD.plot

#' AddLookedUpMetaData
#'
#' Add meta-data into seuratobj@meta.data, looked up in named vector or
#' data.frame
#' @param obj Seurat object
#' @param meta.col existing column of the Seurat meta.data table used to
#'   identify the cells that receive the new values; must exist
#' @param new.col new column name for the Seurat meta.data table to
#'   receive the meta data.  If it exists it is overwritten.
#' @param lookup Named vector (or single-column data.frame *with
#'   rownames*) of new values to add to the Seurat meta.data. Names must
#'   be as in `meta.col`.
#' @param replace.missing If not NULL, use this value for ids not in
#'   names(lookup), otherwise fail
#' @param ignore.superfluous If FALSE, fail on values whose name is not
#'   in meta.col
#' @return Seurat object
#' @examples
#' \dontrun{
#' clustypes <- c(`0`='fibroblast', `1`="NPC", `2`="NPC", `3`="NPC", `4`="T-cell") # note: clusternames are strings
#' ## (the backticks because plain digits are not legal names in R)
#' obj <- AddLookedUpMetaData(obj,
#'                          meta.col="SCT_snn_res.0.8",
#'                          new.col='provisional_type',
#'                          lookup=clustypes,
#'                          replace.missing="UNK")
#' }
AddLookedUpMetaData <- function(obj,
                                meta.col='orig.ident',
                                new.col,
                                lookup,
                                replace.missing=NULL,
                                ignore.superfluous=TRUE
                                ) {
  meta <- obj@meta.data
  assertChoice(meta.col, colnames(meta))
  assertCharacter(new.col, len=1)
  
  if (is.data.frame(lookup)) { # when using e.g. samplesheet[, 'liveness_cutoff', drop=FALSE ] 
    assertDataFrame(lookup, min.rows=1, ncols=1)
    nms <- rownames(lookup)
    lookup <- lookup[[1]]
    names(lookup) <- nms
  }

  assertAtomic(lookup, min.len=1)
  assertNames(names(lookup))
  ids <- unique(meta[[ meta.col ]])
  if (!is.null(replace.missing)) {
    missing <- setdiff(ids, names(lookup))
    m <- rep(replace.missing, length(missing))
    names(m) <- missing
    lookup <- c(lookup, m)
  }
  if(ignore.superfluous) {
    d <- setdiff(names(lookup), ids)
    if(length(d)>0) {
      a <- lookup[d]
      msg <-apply(matrix(c(names(a), a), ncol=2), 1, function(p)sprintf("%s=>%s", p[1],p[2]))
      warning("Ignoring ", paste(sep=";", msg))
    }
    lookup <- lookup[ intersect(names(lookup), ids) ]
  }
  stopifnot(setequal(names(lookup), ids))
  if (new.col %in% colnames(meta))
    cat("will overwrite column ",new.col, "\n")
  else
    cat("will create column ",new.col, "\n")

  newvals <- rep(NA, nrow(meta))
  for(id in ids)
    newvals[ meta[[meta.col]] == id ] <- lookup[id]
  return(AddMetaData(obj, col.name=new.col, metadata=newvals))
} # AddLookedUpMetaData 

#' Add coordinates from different dimensional reductions under another name
#'
#' It is often useful to be play with different dimensional reductions,
#' this convenience function imports the coordinates into a Seurat object
#' for use by e.g. DimPlot and FeaturePlot
#' @param obj Seurat object
#' @param reduction.name  Name to use for this new reduction. Use this name
#'   for the `reduction` argument of FeaturePlot, DimPlot etc.
#' @param coords  The coordinates to use. They should have the same rows
#'   as that of obj@meta.data (order does not matter)
#' @param assay Given as assay name of the resulting DimRed (with NULL
#'   meaning DefaultAssay(obj)), but otherwise meaningles.
#' @return A Seurat object
#' @seealso: DefaultDimReduc, DimPlot, FeaturePlot
AddDimReduc <- function(obj, reduction.name="addedDR", coords, assay=NULL) {
  
  if(! setequal(rownames(coords), colnames(srat)))
    stop("ImportDimReduc: cell names of obj and coords don't match ")
  
  ## following is simply cheap way to add fully-fledgesd DimRed slot
  obj <- RunPCA(obj, reduction.name=reduction.name,
                reduction.key=paste0(reduction.name,"_"),
                npcs=ncol(coords), assay=assay, verbose=FALSE)
  colnames(coords) <- colnames(obj@reductions[[reduction.name]]@cell.embeddings )
  obj@reductions[[reduction.name]]@cell.embeddings <- coords[ colnames(obj), ] 
  obj
} # AddDimReduc
#' DimredToMetadata
#'
#' Transfer the coordinates of the dimensionality reduction to the meta.data for ease of use.
#' By default, meta.data simply gets extra columns 'x' and 'y' added.
#' If the meta.data already contains a column of the desired name,  it is
#' overwritten. 
#' @param obj  The Seurat object 
#' @param dimred  Which dimensionality reduction to use
#' @param fmt     sprinft-format string to use to create the new name. Default leads to just 'x' and 'y'
#' @return The Seurat object
DimredToMetadata <- function(obj,
                             dimred='umap',
                             fmt="%s",
                             dims=1:2) {
  stopifnot(identical(rownames(obj@meta.data), colnames(obj)))
  r <- obj@reductions
  check_choice(dimred, names(r))
  emb <- r[[dimred]]@cell.embeddings
  stopifnot(ncol(emb)==2)
  names <- sprintf(fmt, c('x', 'y'))
  obj@meta.data[, names] <- emb
  obj
} #DimredToMetadata


# Local variables:
# mode: R
# ess-indent-level: 2
# End:


