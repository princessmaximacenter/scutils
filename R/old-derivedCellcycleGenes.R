.plot_derived <- function(CC.cors,
                          S.derived,
                          G2M.derived,
                          Sgenes,
                          G2Mgenes,
                          mindiff,
                          S.cutoff,
                          G2M.cutoff,
                          Scors,
                          G2Mcors,
                          adjust, 
                          scatter.box.ratio,
                          title
                          ) { 
  ## some bits remain hardcoded for now
  lwd <- 0.5
  lty <- 1
  cex <- 2

  ## extend for plotting
  CC.cors$id <- rownames(CC.cors)
  CC.cors$isS <- FALSE
  CC.cors$isG2M <- FALSE
  CC.cors[ Sgenes, 'isS'] <- TRUE
  CC.cors[ G2Mgenes, 'isG2M'] <- TRUE
  
  CC.cors$isSderived <- FALSE
  CC.cors$isG2Mderived <- FALSE
  CC.cors[ S.derived, 'isSderived'] <- TRUE
  CC.cors[ G2M.derived, 'isG2Mderived'] <- TRUE

  CC.cors$highlight <- with(CC.cors, isS+isG2M+isSderived+isG2Mderived)

  ## inside:
  CC.cors$size <- cex
  CC.cors$alpha <- ifelse(CC.cors$highlight, 1, 0.1)
  CC.cors$fill <- 'black'
  CC.cors[ with(CC.cors, isSderived|isG2Mderived),'fill'] <- 'grey'
  CC.cors[CC.cors$isS,'fill']  <- 'red'
  CC.cors[CC.cors$isG2M,'fill']  <- 'blue'

  ## outline:
  CC.cors$color <- 'black'
  CC.cors[CC.cors$isSderived,'color']  <- 'red'
  CC.cors[CC.cors$isG2Mderived,'color']  <- 'blue'
  CC.cors$stroke <- 0
  CC.cors[ with(CC.cors, isSderived|isG2Mderived),'stroke']  <- 0.5*cex
  
  CC.cors <- CC.cors[ order( CC.cors$highlight),  ] # so they're plotted last

  scatter.axis.title.size <- 15
  boxplot.axis.text.size <- 10

  ## set up fake data for the legend (put in bottom left of the grid)
  llim <- c(-1,1)
  df <- data.frame(x=NA_real_,y=NA_real_,
                   score=c('G2M', 'S'),
                   type=c('known','derived'))
  legend <-
    ggplot(df) +
    coord_fixed(xlim=llim, ylim=llim, expand=FALSE) +  
    geom_point(aes(x=x,y=y, colour=score, shape=type)) +
    scale_colour_manual(name="Score:", values=c(S='red', G2M='blue')) + 
    scale_shape_manual(name="Type:", values=c(known=19, derived=1)) +  
    theme_bw() +
    theme(plot.title= element_blank(),
          axis.title= element_blank(),
          axis.line = element_blank(),
          axis.ticks = element_blank(),
          axis.text = element_blank(),
          legend.title = element_text(size=6),
          legend.text = element_text(size=4),
          legend.background=element_rect(fill='white',color='white'),
          legend.position=c(0.5, 0.5),
          legend.box="horizontal",
          legend.direction="vertical",
          legend.spacing = unit(0.1, "cm"),
          legend.box.spacing = unit(0, "cm"),
          legend.box.margin = margin(0, 0, 0, 0, "cm"),
          legend.margin = margin(0, 0, 0, 0, "cm")
          ) +
    guides(color = guide_legend(order = 1),
           shape = guide_legend(order = 2)) +
    coord_fixed(xlim=llim, ylim=llim, expand=FALSE)
  
  scatter <- ggplot(CC.cors, aes(x=G2M, y=S, 
                                 size=size, alpha=alpha,
                                 fill=fill, color=color, stroke=stroke)) +
    coord_fixed(xlim=llim, ylim=llim, expand=FALSE) +  
    ggtitle(title) +
    xlab("correlation against G2M score") +
    ylab("correlation against S score") +
    scale_x_continuous(position = "top", limits=llim) +  
    scale_y_continuous(position = "right", limits=llim) +
    NoLegend() +
    theme_bw() +
    ## theme(axis.title.x=element_text(angle=180)) + # works (but is wrong)
    theme(axis.title.y=element_text(angle=90), ## does not work !@#
          plot.title=element_text(hjust=0.2, face="bold", size=15),
          axis.title=element_text(size=scatter.axis.title.size)
          ) +
    geom_hline(yintercept=0, col='grey') + 
    geom_vline(xintercept=0, col='grey') + 
    geom_abline(intercept= c(-mindiff, mindiff),
                slope=1, col='grey', lwd=lwd, lty=lty) + 
    geom_hline(yintercept = S.cutoff, lty = lty, col = "red", lwd=lwd) + 
    geom_vline(xintercept = G2M.cutoff, lty = lty, col = "blue", lwd=lwd) +
    geom_point(shape=21) +
    scale_size_identity() + 
    scale_alpha_identity() + 
    scale_fill_identity() + 
    scale_color_identity()
  ##      scale_stroke_identity() not needed?!?!
  
  ## boxplots in margin:
  Scors <- Scors[c("all genes", "G2M genes", "S genes")]
  G2Mcors <- G2Mcors[c("all genes", "S genes", "G2M genes")]
  Scors.df <- stack(Scors); names(Scors.df) <- c('values','correlations')
  G2Mcors.df <- stack(G2Mcors); names(G2Mcors.df) <- c('values','correlations')

  Sbox <-
    ggplot(Scors.df, aes(x=correlations, y=values)) +
    scale_x_discrete(position = "top") +  
    geom_boxplot(outlier.size=0.2*cex) +  # or density?
    geom_point(data=data.frame(x=rep(1, length(S.derived)),
                               y=CC.cors[S.derived,'S']),
               colour='red', shape=21, fill='grey', stroke=1,
               aes(x=x,y=y)) + guides(colour="none") + 
    geom_hline(yintercept=S.cutoff, lwd=lwd, lty=lty, color='red') +
    NoLegend() +
    theme_bw() + 
    theme(axis.line=element_blank(),
          axis.text = element_text(size=boxplot.axis.text.size),
          axis.ticks =element_blank(),
          axis.title.y=element_blank(),
          axis.text.y=element_blank(),
          axis.text.x = element_text(angle = 270)
          ) + 
    coord_cartesian(ylim = llim, expand=FALSE)
  
  G2Mbox <-
    ggplot(G2Mcors.df, aes(x=correlations, y=values)) +
    scale_x_discrete(position = "top") +
    geom_boxplot(outlier.size=0.2*cex) +
    geom_point(data=data.frame(x=rep(1, length(G2M.derived)),
                               y=CC.cors[G2M.derived,'G2M']),
               colour='blue', shape=21, fill='grey', stroke=1,
               aes(x=x,y=y)) + guides(colour="none", fill="none") + 
    geom_hline(yintercept=G2M.cutoff, lwd=lwd, lty=lty, col='blue') + 
    NoLegend() +
    theme_bw() + 
    ggtitle(NULL) + 
    theme(axis.line=element_blank(),
          axis.text = element_text(size=boxplot.axis.text.size),
          axis.ticks =element_blank(),
          axis.title.x=element_blank(),
          axis.text.x=element_blank()
          ) +
    coord_cartesian(ylim=llim+adjust, expand=FALSE) + 
    coord_flip(ylim=llim+adjust, expand=FALSE)
  
  plot <- Sbox   +  scatter + 
    legend +  G2Mbox + 
    plot_layout(ncol=2, nrow=2, 
                widths=c(1, scatter.box.ratio),
                heights=c(scatter.box.ratio,1))
  return(plot)
}

#' derivedCellcycleGenes
#'
#' find and plot additional genes that correlate with cellcycle
#' genes. This is done to achieve better deconfounding. Note: this code is now
#' depracated, see \code{\link{derivedCellcycleGenes}} for a better version.
#' @param Scor,G2Mcor Named vectors of correlations per gene, over cells, to S.Score and
#'   G2M.Score (themselves returned from
#'   `metadataCorrelations`). Vectors must be named by gene name.
#' @param Sgenes,G2Mgenes Genes constituting the S- and G2M-specific
#'   genes.  (best use the same ones as used with
#'   `Seurat::CellcycleScoring()` ).
#' @param quantile Genes correlating stronger than this quantile of the
#'   'own' correlations are returned (but see `mindiff`). The same
#'   quantile is used for S and G2M gene correlations (the cutoffs
#'   themselves can easily differ by a factor of two).
#' @param mindiff The absolute difference between the S and G2M
#'   correlations must be more than this for a derived gene to be kept
#'   as additional gene.
#' @param title Title for the plot
#' @param xylim Limit the plot to correlations within this bracket.  For
#'   the horizontal boxplots, this vector is adjusted using the
#'   \code{adjust} argument, see there.
#' @param scatter.box.ratio Ratio of width (height) of the marginal
#'   plots vs the main scatter plot.
#' @param adjust Adjustment to get alignment of the horizontal boxplots
#'   correct (if you know how to do this in ggplot2 please let me know).
#'   The adjustments are added to the xylim argument prior to layout.
#'   The default values work OK for the default xylim, for other xylim
#'   this needs to optimized manually (blue lines should line up, and
#'   x==0 line should line up with median of the 'all genes' boxplot).
#' @return A \code{list(S.derived, G2M.derived, plot)} of additional
#'   genes, as well as a ggplot object containing a scatter plot of the
#'   correlations with boxplots of the marginal correlations of all
#'   genes, S genes and G2 genes versus the corresponding axes.
#' @note Non-cellcycle related genes are transparent, but Seurat's
#'   S.Score and G2M.Score (calculated by \code{AddModuleScore})
#'   occasionally come out identical for certain genes. These will
#'   therefore 'stack' and appear as one dark dot.
#' 
#'   It's difficult to align the axes of the boxplots with those of the
#'   scatterplot; the default \code{xylim} works OK though.
#'   
#'   Another caveat: this code is not optimal, the regression line is
#'   a bit off. This will be addressed soon.
#' @seealso \code{\link{derivedCellcycleGenes2}}, \code{\link{metadataCorrelations}, \link{genomedata}}
#' @author Philip Lijnzaad based on idea by Thanasis Margaritis
#' @examples
#' \dontrun{
#' ## find genes correlating strongly with S.score and G2M.score
#' Scor <- metadataCorrelations(srat, 'S.Score')
#' G2Mcor <- metadataCorrelations(srat, 'G2M.Score')
#' derived <- derivedCellcycleGenes(Scor=Scor,
#'                                  Sgenes=genelists$s.genes,
#'                                  G2Mcor=G2Mcor,
#'                                  G2Mgenes=genelists$g2m.genes)
#' ## draw the correlation scatter plot:
#' show(derived$plot)
#' ## add the newly found genes for general use:
#' genelists <- c(genelists, derived[ c("S.derived", "G2M.derived")])
#' ## exclude the newly found genes from the VariableFeatures:
#' VariableFeatures(srat, assay='SCT') <- 
#'     setdiff( VariableFeatures(srat, assay='SCT'),
#'        unlist(derived[ c("S.derived", "G2M.derived")]))
#' }
derivedCellcycleGenes <- function(Scor, Sgenes,
                                   G2Mcor, G2Mgenes,
                                   quantile=0.25,
                                   mindiff=0.05,
                                   title="Additional cell cycle genes",
                                   xylim=c(-0.2001, 0.8001),
                                   scatter.box.ratio=8,
                                   adjust=c(-0.05, 0.05)) {
  warning("**** SCutils::derivedCellcycleGenes is now depracated, better use derivedCellcycleGenes2 ****\n")
  stopifnot(is.character(names(Scor)) && is.character(names(G2Mcor)))
  stopifnot(identical(names(Scor), names(G2Mcor)))
  stopifnot(is.character(Sgenes) && is.character(G2Mgenes))
  
  allgenes <- names(Scor) # all genes in srat object
  
  extra <- setdiff(Sgenes, allgenes)
  if(length(extra) > 0) {
    warning("**** Got S genes not present in the correlations, ignoring them: ", extra, "****")
    Sgenes <- intersect(Sgenes, allgenes)
  }

  extra <- setdiff(G2Mgenes, allgenes)
  if(length(extra) > 0) {
    warning("**** Got G2M genes not present in the correlations, ignoring them: ", extra, "****")
    G2Mgenes <- intersect(G2Mgenes, allgenes)
  }
  
  cors <- list(Sall = Scor,
               S_Sgenes = Scor[Sgenes],
               S_G2Mgenes = Scor[G2Mgenes],
               
               G2Mall = G2Mcor,
               G2M_G2Mgenes = G2Mcor[G2Mgenes],
               G2M_S = G2Mcor[Sgenes]
               )

  ## find cutoffs based on quantiles. Make sure they don't overlap (too much)
  Scors <- cors[ c('Sall', 'S_Sgenes', 'S_G2Mgenes') ]
  names(Scors) <- c('all genes', 'S genes', 'G2M genes')
  S.cutoff <- quantile(Scors[['S genes']], quantile)
  
  G2Mcors <- cors[ c('G2Mall', 'G2M_G2Mgenes', 'G2M_Sgenes') ]
  names(G2Mcors) <- c('all genes', 'G2M genes', 'S genes')
  G2M.cutoff <- quantile(G2Mcors[['G2M genes']], quantile)

  CC.cors <- data.frame(cors[c('Sall', 'G2Mall')])
  names(CC.cors) <- c('S', 'G2M')
  
  S.derived <- rownames(subset(CC.cors,
                               S > S.cutoff & S - G2M >= mindiff))
  S.derived <- setdiff(S.derived, Sgenes)

  G2M.derived <- rownames(subset(CC.cors,
                                 G2M > G2M.cutoff & G2M - S >= mindiff))
  G2M.derived <- setdiff(G2M.derived, G2Mgenes)

  p <- .plot_derived(CC.cors,
                     S.derived=S.derived,
                     G2M.derived=G2M.derived,
                     Sgenes=Sgenes,
                     G2Mgenes=G2Mgenes,
                     mindiff=mindiff,
                     S.cutoff=S.cutoff,
                     G2M.cutoff=G2M.cutoff,
                     Scors=Scors,
                     G2Mcors=G2Mcors,
                     adjust=adjust,
                     scatter.box.ratio=scatter.box.ratio,
                     title=title
                     )
  return(list(S.derived=S.derived, G2M.derived=G2M.derived, plot=p))
} # derivedCellcycleGenes
