## 10X related functions

#' fast_merge_sparse
#'
#' Fast column-wise merge of sparse matrices, as needed when combining
#' gene x cell matrices
#' @param list_matrices A list() of sparse matrices. Rows are genes,
#'   columns are cells.  Both dimensions should have names. Gene names
#'   are shared across the matrices, cell names must be unique.
#'   Matrices cannot contain all-zero rows, nor all-zero columns.
#'   (filter beforehand!)
#' @return A sparse matrix with rows: union of all genes and
#'   columns: all cells. 
#' @author Based on https://stackoverflow.com/a/52236148/442359
fast_merge_sparse = function(list_matrices) {
  lapply(list_matrices, function(l)if(is.null(rownames(l)))stop("matrix has no rownames"))
  lapply(list_matrices, function(l)if(is.null(colnames(l)))stop("matrix has no column names"))
  lapply(list_matrices, function(l)if(any(Matrix::colSums(l)==0))stop("matrix has all-zero columns"))
  lapply(list_matrices, function(l)if(any(Matrix::rowSums(l)==0))stop("matrix has all-zero rows"))
  allRownames <- sort(unique(unlist(lapply(list_matrices,rownames))))
  if(sum(duplicated(unlist(lapply(list_matrices,colnames))))!=0)
    stop("column names must be unique across all matrices")

  if ( length(list_matrices) == 1)
    return( list_matrices[[1]] )
  
  i <- 1
  for (currentMatrix in list_matrices) {
    message("Merging ", i, "/", length(list_matrices), "\n")
    newRowLocations <- match(rownames(currentMatrix),allRownames) # translate current row pos to pos in new matrix
    indexes <- Matrix::which(currentMatrix>0, arr.ind = TRUE) # only keep non-zeroes
    newRows <- newRowLocations[ indexes[,1] ]
    rm(newRowLocations); gc()
    cols <- indexes[,2]
    newMatrix <- sparseMatrix(i=newRows,j=cols, x=currentMatrix@x,
                              dims=c(length(allRownames), max(cols)))
    rm(newRows)
    colnames(newMatrix) <- colnames(currentMatrix)
    if (!exists("matrixToReturn"))
      matrixToReturn <- newMatrix
    else
      matrixToReturn <- cbind2(matrixToReturn,newMatrix)
    rm(newMatrix)
    gc()
    i <- i+1
  }
  rownames(matrixToReturn) <- allRownames
  matrixToReturn  
} # fast_merge_sparse

#' read10xlibs
#'
#' Read 10x data sets from the 'filtered_feature_bc_matrix.h5' (or
#' similar, like feature_bc_matrix/ ) file(s) and return a raw Seurat
#' object. For gene expression data, metadata "pct_mito", "log2_counts",
#' "log2_features" and counts for mitochondrial genes are read to
#' determine the mitochondrial percentage (pct_mito), but not retained
#' in the Seurat object. If there are no mitochondrial reads in the
#' object (e.g. when it was saved from a project not having them), this
#' is warned about and NA's are used instead.  The cell ids (barcodes)
#' are stripped of any leading '.*_' (e.g. library name, which may be
#' wrong) any trailing "-\[1-9\]", and prefixed with the library name
#' (e.g. 'LX059_'), either from the \code{names} argument or inferred
#' from the \code{files} argument. The `meta.data$orig.ident` column is
#' set to what is described under the \code{names} argument (LX059 in
#' the above example). The genes in resulting object are the union of
#' all genes in all libraries. Genes with no transcript in any cell of
#' any library are not automatically skipped (see arguments
#' \code{min.cells} and \code{min.features} to function
#' \code{CreateSeuratObject} to make that happen)
#' 
#' @param files A (preferably named) vector of h5 files, or directories
#'   containing barcodes.tsv.gz, features.tsv.gz matrix.mtx.gz. E.g.
#'   /data/groups/scgenomics/expression/LX059/filtered_feature_bc_matrix
#' @param names Names to use as prefix for the cellids and for looking
#'   up stuff in the libinfo argument.  They should 'run parallel' with
#'   files. If names is NULL, the names of the \code{files} argument are
#'   used. If neither is given/present, the parent dir of the data file
#'   or directory (LX059 in the example) is used.
#' @param mito_pattern How to recognize mitochondrial reads. It is matched
#'   case-insensitively. The default works for human and mouse.
#' @param libinfo Dataframe with metadata per library, all of which are
#'   added as meta.data to all cells of this library. The data.frame
#'   must have rownames that are a superset of the `names` parameter in
#'   order for the lookup to work. If no libinfo is given, meta.data
#'   will only have the columns orig.ident, nCount_RNA, nFeature_RNA,
#'   pct_mito, log2_counts and log2_features
#' @param type If files is a directory containing not just expression data but also
#'   e.g. multiplexing capture data, \code{\link{Read10X}} returns a list with matrices
#'   for all the data found (and warns about it). In that case, the \code{type} argument
#'   determines which of the actual matrices should be used.
#' @param assay.version  Which assay version to use (either 'v3' or 'v5')
#' @param keep_mito   By default, mitochondrial genes are removed from the object,
#'   this option retains them
#' @param ...  Further arguments (e.g. min.cells) are passed on to
#'   CreateSeuratObject
#' @return a Seurat object. Its meta.data has columns orig.ident,
#'   and (for gene expression data), also pct_mito, log2_counts,
#'   log2_features and whatever was in libinfo.
#' @examples \dontrun{
#'   ## specify files:
#'   files <- c("data/LX059/filtered_feature_bc_matrix.h5",
#'              "data/LX060/filtered_feature_bc_matrix.h5")
#'   ## The above results in orig.ident being "LX059" and "LX060", respectively.
#'   ## To use "TX067" and "TX068" instead, use a named vector:
#'   files <- c(TX067 = "data/LX059/filtered_feature_bc_matrix.h5",
#'              TX068 = "data/LX060/filtered_feature_bc_matrix.h5")
#'   ## 
#'   srat <- read10xlibs(files=files, mito_pattern='^mt-',
#'                       libinfo = df[c('sample', 'species', 'treatment')],
#'                       min.cells=3)
#' ## here, df is some data.frame containing info per library, see docu
#' ## on libinfo
#' }
#' @note The `files` argument should normally be an HDF5 file (typically
#'   named `filtered_feature_bc_matrix.h5`) but for backward
#'   compatibility it can also be a directory containing the files
#'   matrix.mtx.gz, features.tsv.gz and barcodes.tsv.gz (typically named
#'   `filtered_feature_bc_matrix`). This is discouraged because it
#'   doubles the disk usage. 
#' Another caveat: this function is slowly becoming obsolete as it was
#' written for plain gene expression data, not multimodal data such as
#' Feature Barcoding.  For the latter, it should be able to read the
#' Gene Expression part of the data, but was not yet tested for e.g. the
#' "Antibody Capture" part.
#' 
#' @seealso CreateSeuratObject, DropletUtils::write10xCounts.
#' @author plijnzaad@gmail.com, partly based on code by Jeff de Martino
#' 
read10xlibs <- function(files,
                        names=NULL,
                        mito_pattern="^MT-",
                        libinfo=NULL,
                        type='Gene Expression',
                        assay.version='v3',
                        keep_mito = FALSE,
                        ...) {
  library(Seurat)
  options(Seurat.object.assay.version = assay.version)
  stopifnot(!any(duplicated(files)))
  
  if(is.null(names))
    names <- names(files)
  
  if(is.null(names))
    names <- basename(dirname(files))  
  
  if( any(duplicated(names)) )
    stop("Could not infer unique names for the data sets from their file names,
please use a named vector for the  files argument. Current inferred names are:\n",
paste(collapse=" ", names))
  
  stopifnot(length(files)==length(names))
  
  if(!is.null(libinfo)) 
    if(! all( names %in% rownames(libinfo)))
      stop("cannot lookup data in libinfo, names don't match")

  # Create a metadata list with fields not generated by Seurat
  message("Preprocessing ...")
  
  gex <- (type=='Gene Expression') 
  
  data_list <- list()
  for (i in seq_along(files) ) {
    file <- files[i]
    lib <- names[i]
    message("Reading ", lib , " from ", file)
    if( grepl("\\.h(df)?5$", file, ignore.case=TRUE) ) # a file, not a dir but hey.
      counts <- Read10X_h5(filename=file)
    else { 
      counts <- Read10X(data.dir=file)
    }
    if(is.list(counts)) {
      expect_choice(type, names(counts))
      counts <- counts[[type]]
    }
    ## throw out all-zero rows and columns
    counts <- counts[ Matrix::rowSums(counts)!=0 , Matrix::colSums(counts)!=0  ]
    gc()
    message("Found ",  paste(collapse=" x ", dim(counts)), " (features x droplets)")
    barcodes <- gsub(".*_","",colnames(counts)) # may be the wrong libname, use our own.
    barcodes <- gsub("[-_0-9]*$","",barcodes) # trailing '-1'
    stopifnot(sum(duplicated(barcodes))==0)
    colnames(counts) <- paste(sep="_", lib, barcodes)
    data_list[[lib]] <- counts
    rm(counts)
    gc()
  }

  if (gex) { 
    ## also collect the meta data such as pct_mito:
    meta_list <- lapply(names(data_list),function(lib) {
      counts <- data_list[[lib]]
      ncells <- ncol(counts)
      mitos <- grepl(mito_pattern, rownames(data_list[[lib]]),
                     ignore.case=TRUE)
      if(sum(mitos)== 0) { 
        warning("**** Found no mitochondrial genes, pct_mito cannot be calculated ****")
        pct_mito <- rep(NA_real_, ncells)
      } else {
        pct_mito <- 100 * Matrix::colSums(counts[ mitos , ]) /
          Matrix::colSums(counts)
      }
      
      log2_transcript_counts <- log2(1 + Matrix::colSums(counts[ !mitos, ]))
      log2_feature_counts <- log2(1 + Matrix::colSums(counts[ !mitos, ] > 0))
      df <- data.frame("pct_mito" = pct_mito,
                       "log2_counts" = log2_transcript_counts,
                       "log2_features" = log2_feature_counts)
      if(!is.null(libinfo)) {
        extra <- as.data.frame(lapply(libinfo[lib,],
                                      function(col){rep(col, ncells)}))
        df <- cbind(df, extra)
      }
      df
    })
    names(meta_list) <- names(data_list)
    meta_merged <- meta_list %>% dplyr::bind_rows()
    gc()
  } else { meta_merged <- NULL }
  
  ## concatenate the data properly:
  data_merged <- fast_merge_sparse(data_list)
  rm(data_list)
  gc()

  if (gex && !keep_mito) {
    data_merged <- data_merged[ !grepl(mito_pattern, rownames(data_merged),
                                       ignore.case=TRUE),  ]
    gc()
  }  
  
  # Create Seurat object and filter
  message("Creating Seurat object and filtering...")
  srat <- suppressWarnings(Seurat::CreateSeuratObject(data_merged, 
                                                      meta.data = meta_merged, ... ))
  gc()
  return(srat)
} #read10xlibs

#' prepare.10x.annotation
#'
#' Create data.frame with the essential gene annnotation and write it to disk. Real work is done by
#'   species-specific functions. 
#' @param gtf.file   Where to get the info from. It assumes the kind of GTF file that is produced by cellranger mkref
#'   but may work regardless. If NULL, a default (see source code for inspiration) is taken
#' @param gene.name.disambiguations  File to resolve non-unique names (needed for human)
#' @param species   Currently just 'human' or 'mouse'
#' @param output.rds   Name of output file. If NULL, a default is chosen based on the name of the gtf.file argument.
prepare.10x.annotation <- function(gtf.file=NULL, # for default see below
                                   gene.name.disambiguations=NULL,
                                   output.rds=NULL,
                                   species='human') {
  library(rtracklayer)

  if(is.null(output.rds)) {
    root <- gsub("\\.g[tf]f.*", "", basename(gtf.file))
    output.rds <- paste0( root, ".rds")
  }
  
  if(species == 'human') {
    if ( is.null(gtf.file) )
      gtf.file <- "~/data/refdata/cellranger-GRCh38-3.0.0-genes.gtf.gz"
    if ( is.null(gene.name.disambiguations) )
      gene.name.disambiguations <- "~/git/sc-facility/refdata/cellranger-GRCh38-3.0.0-disambiguation.txt"
    return( .prepare10x.human(gtf.file, gene.name.disambiguations, output.rds) )
  }
  
  if(species == 'mouse') {
    if(is.null(gtf.file)) 
      gtf.file <- "/hpc/local/CentOS7/gen/data/10X-refdata/refdata-gex-mm10-2020-A/genes/genes.gtf"
    return( .prepare10x.mouse(gtf.file, output.rds) )
  }
  stop("Dont' now how to prepare annotation for species ", species)
} #prepare
  
.prepare10x.human <- function(gtf.file="~/data/refdata/cellranger-GRCh38-3.0.0-genes.gtf.gz",
                              gene.name.disambiguations="~/git/sc-facility/refdata/cellranger-GRCh38-3.0.0-disambiguation.txt",
                              output.rds,
                              species='human') {
  gtf <- rtracklayer::import(gtf.file) # 2565061
  gtf <- subset(gtf, type=='gene') # 33538 (i.e. just the full-length features, no exons or txpts)
  stop.unless.unique(gtf$gene_id)

  ## disambiguates and adds Entrez ids

  ## first add the Entrez numbers (as far as possible; many A[CL]\d+, linc and antisense are missing)
  anno <- data.frame(gtf); rm(gtf)
  
  missing.name <- is.na(anno$gene_name) # gene_name is missing for our own constructs, copy gene_id
  anno[ missing.name, 'gene_name' ]  <- anno[ missing.name, 'gene_id' ]
  if(any(missing.name))
    warning(sum(missing.name), " gene_names missing, using gene_id for them")
  rownames(anno) <- anno$gene_id # make unique using ensids, later overwrite them with HGNC names

  anno <- add.entrez_id(anno, species=species)
  
  g <- anno$gene_name
  ## table contains duplicate ids (!?), needs to be made unique
  dups <- g[duplicated(g)] # 24
  s <- subset(anno, gene_name %in% dups)
  s[which(unlist(lapply(s, function(col)all(is.na(col)))))] <- NULL # get rid of NA cols
  ## This gives a table with duplicates; save it and manually add a column 'what' containing
  ## 'keep' or 'rename', indicating which ones to keep, which to amend. Then read it back in:
  duptable <- read.table(gene.name.disambiguations,
                         comment.char="#", header=TRUE, sep="\t")
  stopifnot(setequal(duptable$gene_name, s$gene_name)) # otherwise: redo the manual resolution
  stopifnot(setequal(duptable$what, c("keep", "rename")))
  for(name in duptable$gene_name) { 
    dups <- subset(duptable, gene_name == name)
    stopifnot(sum(dups$what== "keep")==1)
  }
  
  ## disambiguate by renaming the less trusty ones with ENSG000xx--HGNCname
  duptable$id  <- duptable$gene_name
  duptable[ duptable$what == "rename", 'id'] <- with(subset(duptable, what == "rename"), 
                                                     paste(sep="--", gene_id , gene_name))
  stop.unless.unique(duptable$id)

  rownames(duptable) <- duptable$gene_id
  anno[which(unlist(lapply(anno, function(col)all(is.na(col)))))] <- NULL # get rid of NA cols
  anno[c("source", "gene_version", "gene_source" )] <- NULL
  anno$id <- anno$gene_name
  anno[ rownames(duptable), 'id' ] <- duptable$id
  stop.unless.unique(anno$id)
  rownames(anno) <- anno$id # both row name and id

  warning("writing output to ", output.rds)
  saveRDS(obj=anno,file=output.rds)
  rm(duptable,anno)
  gc()
  return()
} # .prepare10x.human

.prepare10x.mouse <- function(gtf.file,output.rds) { 
  gtf = import(format="gtf", gtf.file, genome="Mus_musculus")
  gtf = gtf[ gtf$type == 'gene', ]
  gc()
  coords <- data.frame(chr=seqnames(gtf),
                       start=start(gtf),
                       end=end(gtf),
                       strand=strand(gtf))
  attribs <- values(gtf)[, c("gene_id", "gene_name", "gene_type", "mgi_id") ]
  
  df <- cbind(coords, attribs)
  df$id <- df$gene_name
  
### MORE HERE, like refseq and GeneID ?

  dups <- duplicated(df$gene_name)
  ## there are 40 of them, 39 also have their MGI_ID duplicated
  
  ## replace with ENSMUSG :
  df[dups, 'id']  <- df[dups, 'gene_id']
  rownames(df) <- df$id
  df$id <- NULL
  warning("writing output to ", output.rds)
  saveRDS(file= output.rds, df)
  gc()
  return()
} # .prepare10x.mouse
