### functions specific for sortseq and platediagnostics (ie. platebased platform).

#' returns name or number of well of single 384-well plate
#' 
#' @param i,j If i and j are NULL, return the names of all wells. If j is NULL, return
#'   character name of well if i is numeric, and numeric index of well if i is character.
#'   If neither are NULL, treat as coordinates and return the numeric index.
#' @return a character vector (if input was numeric) or integer vector (if input was character).
#'   'UNK' is the standard unknown well (arising from misread barcodes). 
#' @examples
#'  wellname(54) # 'C6'
#'  wellname('C6') # 54
#'  wellname(3, 6) # 54
#'  wellname(0) # 'UNK'
#'  # wellname() # "UNK", "A1", "A2", ... "P24"
wellname <- function(i=NULL,j=NULL) {
  wells <- 1:384

  rows <- LETTERS[1:16]
  cols <- 1:24
  m <- matrix(kronecker(X=rows, Y=as.character(cols), FUN=paste0),byrow=TRUE,
              nrow=length(rows),ncol=length(cols), dimnames=list(rows,cols))
  names(wells) <- t(m)
  wells <- c(UNK=0, wells)
  
  if (is.character(i))                # character to numeric idx
    return(.checknotNA( unname(wells[i])))        # unname for clarity
  
  if(is.null(i))                      #return all
    return(.checknotNA(names(wells)))

  stopifnot(is.numeric(i))
  if(any(is.na(i) | i < 0  | i > 384))
    stop("wellname: invalid well specification: ", i)

  if(is.null(j))                      # numeric to character idx
    return(.checknotNA(names(wells[i+1])))
  
  stopifnot(is.numeric(j))
  if(any(is.na(j) | j < 0  | j > 24))
    stop("wellname: invalid well specification: ", j)

  if (any(sum(i==0 | j==0) > 0))
    stop("wellname: wrong usage, cannot use index 0 (reserved for the virtual 'UNK' well)")
  return(.checknotNA(m[i,j]))                                #rarely used
}                                       #wellname

.checknotNA <- function(x){if(any(is.na(x)))stop("Invalid well coordinates"); x}


.known.wellsets <- function(){
  ## define commonly used sets of wells. Access using lookup.wellset
  lst <- list(
    None=character(0),
    col1= wellname(seq(1,361, by=24)),
    col1odd =  wellname(seq(1, 337, by=48)), # i.e. {A,C,E,...,O}1
    col1even = wellname(seq(25, 384, by=48)), # i.e. {B,D,F,...,P}1
    col24= wellname(seq(24,384, by=24)),
    col24odd =  wellname(seq(24, 360, by=48)), # i.e. {A,C,E,...,O}24
    col24even = wellname(seq(48, 384, by=48)), # i.e. {B,D,F,...,P}24
    rowA= wellname(1:24),
    rowAodd= wellname(seq(1,23,by=2)),  # A{1,3,5,...,23}
    rowAeven= wellname(seq(2,24,by=2)), # A{2,4,6,...,24}
    rowP= wellname(361:384),
    rowPodd=wellname(seq(361,383, by=2)), #P{1,3,5,...,23}
    rowPeven=wellname(seq(362,384, by=2)),#P{2,4,6,...,24}
    bottomright8 = wellname(c(357:360,381:384)),
    OPodd=wellname(seq(14*24+1, 383,2)),
      OPeven=wellname(seq(14*24+2, 384,2))
  )
  
  ## add all separate rows and columns
  for (row in 1:16) {
    L <- LETTERS[row]
    name <- paste0("row",L)
    if(is.null(lst[[name]]))
      lst[[name]] <- paste0(L, 1:24)
  }
  
  ## add all separate rows and columns
  for (N in 1:24) {
    name <- paste0("col",N)
    if(is.null(lst[[name]]))
      lst[[name]] <- paste0(LETTERS[1:16], N)
  }
  
  # aliases:
  lst$none <- lst$None
  lst[['NA']] <- lst$None
  lst[['NULL']] <- lst$None
  lst$column1 <- lst$col1
  lst$column24 <- lst$col24
  
  lst 
}                                       # .known.wellsets

.wellcoordinate.syntax.ok <- function(name)
  grepl("^(UNK|([A-P][1-9][0-9]{0,1}))$",name, ignore.case=TRUE, perl=TRUE)

#' known.wellsets
#'
#' Obtain the names of all known wellset shorthands
#' @return A vector of wellset names (e.g c("col1", "col1odd", "col1even" etc.))
#' @seealso ,\code{\link{lookup.wellset}},\code{\link{goodwells}},\code{\link{goodERCCs}}, \code{\link{livecellcutoff}}
known.wellsets <- function(){
  return(names(.known.wellsets()))
}

asc <- function(x) { strtoi(charToRaw(x),16L) }
chr <- function(n) { rawToChar(as.raw(n)) }

.lookup.rows <- function(name) {
  stopifnot(grepl("^[A-P](-[A-P])?$", name, perl=TRUE))
  parts <- unlist(strsplit(name, "-"))
  if (length(parts)==1) # just one row
    return( paste0(parts[1], 1:24))
  stopifnot(length(parts)==2)

  chars <- sapply(asc(parts[1]):asc(parts[2]), chr)
  return( unique(as.vector(outer(chars, 1:24, FUN=paste0))))
} # .lookup.rows

.lookup.cols <- function(name) {
  name <- toupper(as.character(name))
  stopifnot(grepl("^[1-9][0-9]{0,1}(-[1-9][0-9]{0,1})?$", name, perl=TRUE))
  parts <- as.integer(unlist(strsplit(name, "-")))
  if (length(parts)==1) # just one row
    return(paste0(paste0(LETTERS[1:16],parts[1])))
  stopifnot(length(parts)==2)
  return( unique(as.vector(outer(LETTERS[1:16], parts[1]:parts[2],FUN=paste0))))
} # .lookup.cols

#' Lookup sets of wells by their shorthand name or coordinates
#'
#' Within \code{SCutils} and \code{platediagnostics} a number of
#'   shorthands are defined to refer sets of wells to treat as empty or
#'   to igore. This function defines decodes shorthands and coordinates
#'   and translates them to vectors of well names.  To see how to work
#'   with ignored cells, see the example of \code{\link{goodwells}}.
#' @param name If \code{name} is \code{NULL} (or \code{NA} or
#'   \code{""}), an empty character vector is returned. Otherwise, it is
#'   looked up and translated to a character vector of the
#'   well-coordinates. If it is a comma-separated set of well
#'   coordinates, it is syntax-checked and likewise converted to a
#'   character vector of well-coordinates. If it is a defined 
#'   shorthand (such as 'bottomright8'; see \code{\link{known.wellsets}}),
#'   those wellnames are returned. Lastly, the string is parsed as
#'   coordinates of wells, exemplified as follows (leaving out the quotes)
#'   * B =>       whole row B
#'   * B-D =>     all of all rows B-D
#'   * 3 =>       whole column 3
#'   * 3-5 =>     all of columns 3-5
#'   Rows can columns can be combined, separated by ':'; see the examples.
#' @return A string of characters, such as \code{c("A24", "B24",
#'   "C24")}. Note that well coordinates are always uppercase.
#' @seealso \code{\link{goodwells}},\code{\link{goodERCCs}},
#'   \code{\link{livecellcutoff}},\code{\link{known.wellsets}}
#' @examples
#'   lookup.wellset('A1,B2,C3') # comma-separated
#'   lookup.wellset('bottomright8') # shorthand
#'   lookup.wellset('rowPodd') # shorthand
#'   lookup.wellset('A2') # single well
#'   lookup.wellset('B') # single row
#'   lookup.wellset('B-D') # several rows
#'   lookup.wellset('3') # single col
#'   lookup.wellset('3-5') # several cols
#'   lookup.wellset('B:3-5') # part of single row
#'   lookup.wellset('B-D:3') # part of single col
#'   lookup.wellset('B-D:3-5') # rectangle
lookup.wellset <- function(name=NULL) {
  if (is.null(name) ||is.na(name) || name=="" || length(name)==0)
    return(character(0))
  name <- as.character(name)
  name <- gsub(" *", "", name)
  
  if( length(name) !=1 )
    stop("lookup.wellset: can currently only take single string as input, sorry.")
  
  w <- .known.wellsets()
  if (!is.null(w[[name]])) # shorthand like col24
    return(w[[name]])

  parts <- unlist(strsplit(name, ":")) # e.g. A-C:2-4
  if(length(parts)==2)
    return(intersect( .lookup.rows(parts[1]), .lookup.cols(parts[2])))
  if(length(parts)!=1)
    stop("lookup.welsets: wrong syntax for well-coordinates: ", name)
  
  if (grepl("^[A-P](-[A-P])?$", name, perl=TRUE))
    return(.lookup.rows(name))
  if (grepl("^\\d+$", name, perl=TRUE))
    return(.lookup.cols(name))

  ## comma-separated set of single wells:
  lst <- unlist(strsplit(toupper(name), ","))
  if (all( .wellcoordinate.syntax.ok(lst) ))
    return(lst)
  
  stop(paste(collapse="; ", name),
       ": not known as a shorthand, nor a valid set of well-coordinates either")
} #lookup.wellset

.maybe.warn.about.obsolete <- function() {
  if(options()$verbose || Sys.getenv("DEBUG")!="" || Sys.getenv("VERBOSE") !="")
    warning("functions {keep,rm}{spike,mito}() are only for Sharq1 and will become obsolete")
}

.keep <- function(x, pattern, ...){
  .maybe.warn.about.obsolete()
  sel <- grep(pattern, rownames(x), perl=TRUE, ...)
  if(length(sel) == 0) {
    warning(".keep: grepping for ", pattern, " yielded no results, returning full matrix")
  }
  x[ sel,,drop=FALSE];
}

.rm <- function(x, pattern, ...){
  .maybe.warn.about.obsolete()
  sel <- grep(pattern, rownames(x), perl=TRUE, ...)
  if(length(sel)==0) {
    warning(".rm: grepping for ", pattern, " yielded no results, returning full matrix")
    return(x)
  }
  x[ -sel ,,drop=FALSE];
}

#' Extract the ERCCs from a matrix or data.table. Obsolete; needed for Sharq1 pipeline.
#'
#' @param x matrix or data.frame, the rows of which are gene names
#' @param regexp   Perl regexp to recognize spike-ins by
#' @return  smaller matrix or data.frame containing only the genes matching "^ERCC-". If
#'   nothing is found, the full matrix is returned.
keepspike <- function(x, regexp="^ERCC-").keep(x,regexp)

#' Extract all but the ERCCs from a matrix or data.table. Obsolete; needed for Sharq1 pipeline
#'
#' @param x matrix or data.frame, the rows of which are gene names
#' @param regexp   Perl regexp to recognize spike-ins by
#' @return smaller matrix or data.frame containing everything but the
#'   genes matching "^ERCC-". If nothing is found, the full matrix is
#'   returned.
rmspike <- function(x,regexp="^ERCC-").rm(x,regexp)

#' Extract all the mitochondrial genes from a matrix or data.table. Obsolete; needed for Sharq1 pipeline.
#'
#' @param x matrix or data.frame, the rows of which are gene names
#' @param regexp Perl regexp to recognize mitochondrial genes by. Matching is
#'   case-insensitive. The default works fine for Human and Mouse. For
#'   yeast, use "^Q0"
#' @return smaller matrix or data.frame containing only the genes
#'   matching regexp.  If nothing is found, the full matrix is returned.
keepmito <- function(x, regexp="[-_]{2}MT-").keep(x,regexp, ignore.case=TRUE)

#' Extract all but the mitochondrial genes from a matrix or data.table. Obsolete; needed for Sharq1 pipeline.
#'
#' @param x matrix or data.frame, the rows of which are gene names
#' @param regexp   regexp to recognize mitochondrial genes by
#' @return smaller matrix or data.frame containing everything but the
#'   genes matching "__MT-" (matching is case-insenstive; this works
#'   fine for Human and Mouse). If nothing is found, the full matrix is
#'   returned.
rmmito <- function(x, regexp="[-_]{2}MT-").rm(x,regexp, ignore.case=TRUE)

#' Return subset of the ERCC transcripts matrix to use for deciding if a well is good
#' 
#' Deciding wether all the reactions in a well worked (is 'good') is
#' done using ERCC's that are sufficiently abundant; this function
#' selects those ERCCs, in two steps: first find wells that 'obviously
#' failed' (having a total ERCC transcript count below
#' \code{min.req.ERCCs}, and ignore them (as they 'drag down' the median
#' too much). In the remainder of wells, the median should be higher
#' than \code{good.ERCC.median}. Also, there should be at least 2 such good ERCCs.
#' Note that it now better to work with \code{\link{find.wellsets}}.
#' @param ERCCs matrix of transcript counts. rows are ERCC's, columns are wells.
#' @param good.ERCC.median ERCCs having a median greater than this are selected.
#' @param min.req.ERCCs Wells with fewer total ERCC txpts than this
#'     are 'obviously failed' and ignored. Also, there should be at least this many ERCC species
#'     (so this argument is used for two different purposes, but acting in the same 'direction')
#' @seealso \code{\link{find.wellsets}}, \code{\link{goodwells}},\code{\link{lookup.wellset}}, \code{\link{livecellcutoff}}
#' @return A 'horizontal' subset of the matrix of ERCC-txpt counts (as a matrix), or 0x0 matrix if none can be found
goodERCCs <- function(ERCCs,
                      good.ERCC.median=4,
                      min.req.ERCCs=2) {
  if(is.null(good.ERCC.median) || is.null(min.req.ERCCs))
    stop(" PLTAG-aab26e42 ")
  obviously.failed <- colSums(ERCCs) < min.req.ERCCs
  isgood.ERCC <- apply(ERCCs[, !obviously.failed ],1,median, na.rm=TRUE) >= good.ERCC.median
  if ( sum(isgood.ERCC) >= min.req.ERCCs )
    return(  ERCCs[isgood.ERCC,,drop=FALSE] )
  warning("*** Could not find enough good ERCCs ***")
  return(matrix(NA, nrow=0, ncol=0))
}                                       #goodERCCs

#' Return the names of wells where all reactions worked, based on ERCC transcript counts
#'
#' Whether reactions worked is decided using the transcript counts of
#' ERCCs that are abundant enough, so-called 'good ERCCs' (see
#' \code{\link{goodERCCs}} for that). For a well to pass as 'good', two
#' criteria must be met. Firstly, no more than \code{allowfailed} +
#' \code{allowfailed.frac} * ngood of the 'good ERCCs' can be missing
#' (ngood is the number of good ERCCs). Secondly, the log2 of average
#' 'good ERCC' abundance in any well must be greater than \code{
#' median(x) - mad.factor * mad(x) }, where \code{x} is log2 of the
#' average 'good ERCC' abundance in 'really good wells', with 'really
#' good wells' defined as those having fewer than
#' \code{floor(allowfailed.frac * ngood} ERCC transcripts missing.
#' 
#' IGNORING WELLS
#'
#' If cells are to be ignored, both \code{\link{goodERCCs}} and
#' \code{goodwells} have to know about it, because in both cases the
#' outcome might be different if there is a set of cells containing very
#' low or high numbers of transcripts. A typical example of how to deal
#' with ignored cells in actual code is shown in the example below. Note:
#' the example is rendered largely obsolete by the
#' \code{\link{find.wellsets}}; see the example there.
#' @param goodERCCs matrix or data.frame of ERCC (spike-in) transcripts
#'   counts that are deemed 'good'
#' @param allowfailed See above
#' @param allowfailed.frac See above
#' @param mad.factor See above
#' @param ignore  vector of wellnames that must be ignored
#' @return names of wells where reaction worked properly
#' @seealso \code{\link{find.wellsets}}, \code{\link{lookup.wellset}},\code{\link{goodERCCs}}, \code{\link{livecellcutoff}}
#' @examples \dontrun{
#' # get the coordinates of the wellsets
#' wellsets <- list(unk='UNK',
#'                  allknown = wellname()[-1], # ignore first elt which is 'UNK'
#'                  empty <- lookup.wellset('col24uneven'))
#'
#' ignore <- lookup.wellset('OPodd') #or e.g. 'A1,B2,C3' etc.
#'
#' wellsets$ignore <- ignore # for completeness
#' wellsets$wanted <- setdiff(wellsets$allknown, ignore)
#' wellsets$empty <- setdiff(wellsets$empty, ignore)
#' wellsets$full <- setdiff(wellsets$wanted,wellsets$empty)
#' 
#' # define the ERCCs that are good enough to determine which wells
#' # are good enough:
#' good.ERCCs <- goodERCCs(matrix.of.spikeins)
#' 
#' # define the goodwells:
#' good.wells <- goodwells(good.ERCCs)
#' 
#' # add them the wellsets (livecellcutoff needs 'good' and 'empty')
#' wellsets$good <- good.wells
#' wellsets$failed <- with(wellsets, setdiff(all, good))
#' 
#' # livenesscutoff works fine because wellsets$good and wellsets$empty
#' # don't contain any cells to be ignored
#' livecell.cutoff <- livecellcutoff(genes, wellsets)
#' 
#' }
#' 1 # (hack, otherwise example-section not created)
goodwells <- function(goodERCCs,
                      allowfailed=1,
                      allowfailed.frac=0.1,
                      mad.factor=3) {
  ngood <- nrow(goodERCCs)

  noERCCs <- goodERCCs==0
  
  ## 1st criterion: no more than so-many missing good ERCCs in any well:
  allow.missing <- allowfailed + allowfailed.frac*ngood
  ## proposal: allow.missing <- floor(allowfailed.frac*ngood)
  goodwells1 <- apply(noERCCs, 2, function(col)sum(col) <= allow.missing)
  goodwells1 <- names(which(goodwells1)) # any NA's will have gone

  ## 2nd criterion is based on the distribution of ERCCs, relative to
  ## that of ERRCs found in the 'best wells': those that don't have 'too
  ## many' of the good ERCCs missing:
  bestwells <- colSums(noERCCs) <= floor(allowfailed.frac*ngood)
  bestwells <- names(which(bestwells))  #any NA's will have gone
  
  x <- log2(colMeans(goodERCCs[, bestwells,drop=FALSE])+1)
  y <- log2(colMeans(goodERCCs[,          ,drop=FALSE])+1)
  cutoff2 <- median(x)-mad.factor*mad(x)
  goodwells2 <- y>cutoff2
  goodwells2 <- names(which(goodwells2))
  return(intersect(goodwells1,goodwells2))
}                                       # goodwells

#' Returns a cut-off used to judge wether a well contained a live cell
#' 
#' The 'liveness' of the cell in a well is based on the number of gene
#' transcripts in \strong{empty} wells. That is, we use the level of
#' 'contamination' into empty wells to separate noise from signal.
#' Given empty.counts as a vector containing the sum of transcript
#' counts per non-failed empty well (of those genes that are expressed
#' in non-empty wells), then
#' \code{median(empty.counts)+mad.factor*mad(empty.counts)} is a good
#' cutoff. For an example, see under \code{\link{goodwells}}. The cutoff
#' adaptive (i.e. differs per plate) and is used for QC purposes. In a
#' setting with several plates, it is often better to use a fixed, rounded
#' number as a cutoff for all the plates.
#' Note that it's now better to work with \code{\link{find.wellsets}}
#' @param genes matrix or data.frame with transcript counts per gene and
#'   per well. Should not contain ERCCs nor mitochondrial genes
#' @param wellsets List of character vectors containing the names of
#'   various sets of wells.  The sets 'good' (from
#'   \code{\link{goodwells}}) and 'empty' (the empty wells) are used.
#'   (if all's well, neither of these sets contains any of the wells
#'   listed in `wellsets$ignore`).
#' @param mad.factor See above
#' @param min.good.empties The minimum number of required non-failed
#'   emptywells. If below this, a warning is issued and
#'   \code{min.cutoff} is returned.
#' @return the number of transcripts above which those in a non-failed,
#'   non-empty well is indicative of that well containing live cell. If
#'   no reasonable cutoff can be found, \code{NA} is returned.
#' @seealso \code{\link{find.wellsets}}, \code{\link{goodwells}},\code{\link{goodERCCs}},
#'   \code{\link{livecellcutoff}}
livecellcutoff <- function(genes,
                           wellsets,
                           mad.factor=3,
                           min.good.empties=2) {
  if (is.null(wellsets$wantedgood))
    stop("this is a bit obsolete now, wellset 'wantedgood' was expected to have been\n\
defined. Please use function `find.wellsets()` instead.")
  use <- with(wellsets, intersect(wantedgood, empty))

  if(is.null(use) || length(use) < min.good.empties ) {
    warning(sprintf("livecellcutoff: fewer than %d good empty wells, livecellcutoff cannot be trusted", min.good.empties))
    return(NA)
  }
  
  expressed <- rowSums(genes[, wellsets$wantedgood ]) > 0
  empty.counts <- colSums(genes[expressed, use, drop=FALSE])
  m <- median(empty.counts) + mad.factor*mad(empty.counts)
  return(m)
}                                       #livecellcutoff

#' Read count data (such as produced by the tally.pl script)
#'
#' @param file The file to be read. Must be tab delimited, first row
#'   GENEID \\t UNK \\t A1 \\t A2 ... \\t P24, next rows the gene name followed
#'   by the read counts
#' @param keep.unk  If TRUE, do keep the UNK column representing the 'unknown well'
#' @param ignore.wells  If not NULL, throw out these well names. Syntax has
#'   to have valid syntax (eg. "A1,B2") or be a valid shorthand as returned by
#'   \code{\link{known.wellsets()}}
#' @return a matrix  with row- and column names.
#'   The unknown well 'UNK' is ignored by default.
#'   If file is NA or NULL, NULL is returned.
read.counts <- function(file, keep.unk=FALSE, ignore.wells=NULL) {
  if (is.na(file) || is.null(file))
      return(NULL)
  if (!file.exists(file)) {
    warning(file , " not found")
    return(NULL)
  }
  ignore <- lookup.wellset(ignore.wells)
  allwells <- wellname()
  invalid <- setdiff(ignore, allwells)
  if(length(invalid)>0)
    stop("Unknown well specification: ", paste(collapse=" ", invalid))
  x <- read.csv(file=file, header = TRUE, sep = "\t",row.names =1, comment.char="#")
  invalid <- setdiff(colnames(x), allwells)
  if(length(invalid)>0)
    stop("Unknown well names: ",
         paste(collapse=" ", invalid),
         ", Wellnames should be UNK, A1-24, B1-24, ..., P1-P24")
  if(!keep.unk)
    ignore <- c("UNK", ignore)
  wanted <- setdiff(allwells, ignore)
  x <- x[,wanted,drop=FALSE]
  as.matrix(x)
}                                       #read.counts

#' Set various wellsets for given data set.
#'
#' Determines which wells had good or failed reactions (good/failed)
#' based on ERCC content, and forms various subsets of the available
#' wells, also based on which wells are left empty for QC purposes, or
#' should be ignored. The latter is different from empty; ignore'd are
#' wells that are deemed defective (e.g. contain >1 cell) or
#' e.g. contain cells from a different species.
#' 
#' All wells, including ignore'd, are considered when judging
#' wells on a technical level. There are a few divisions that can be
#' made:
#'      
#' * allknown: all 384 wells (as opposed to the virtual unknown well called 'UNK')
#'   * ignored: wells to be ignored by request (regardless of good/failed). Specified by user.
#'   * wanted: all but ignored (regardless of good/failed)
#'     * empty: left empty for QC-purpose. Specified by the user
#'     * full: all the wanted that are not empty.
#'
#' Separate from this is the failed/good distinction:
#'
#' * allknown: 
#'   * good: all wells that had good reactions (regardless of wanted/ignored)
#'   * failed: all wells that did not have good reactions (regardless of wanted/ignored)
#'
#' The wanted category separates along the good/failed lines:
#'
#' * wanted
#'   * wantedgood (regardless of full/empty)
#'   * wantedfailed (regardless of full/empty)
#' 
#' The wanted category is also split along empty/full lines,
#' which is subdivided even further:
#'
#' * wanted
#'   * empty (regardless of failed/good). Specified by user; should not overlap with ignore
#'     * emptygood: empty and good reactions
#'     * emptyfailed: empty and failed reaction
#'   * full: not empty (regardless of failed/good)
#'     * fullgood: not empty and not failed
#'       * live: wells deemed to contain live cells based on biological transcripts
#'       * dead: fullgood that are not live
#'     * fullfailed: not empty but failed
#' 
#' For downstream analysis the most important wells are the 'fullgood' wells. 
#' The live/dead sets are aimed at QC, because when combining several
#' plates for analysis, typically a fixed cutoff on the biological transcripts is used.
#' 
#' @param data   data matrix of read counts. Rows are genes, columns are wells
#' @param empties  short-hand or coordinates of wells considered to be left
#'   empty for control purposes. This is also stored as `$empty.requested`
#'   in the returned result, to distinguish it from `$empty` which are the
#'   good (i.e. non-failed) `$empty.requested`
#' @param ignore  wells that should be ignored (except for anything
#'   relating to the technical quality of a well. There, _all_ wells are considered)
#' @param species  used for finding mitochondrial genes (can be 'human' or 'yeast')
#' @param badERCCs  one of 'use.top3', 'fail'. If == 'use.top3', use those and continue. If 'fail', crash.
#' @param   min_liveness_cutoff  If automatically determined cutoff is lower than this, use this instead.
#' @param  good.ERCC.median,min.req.ERCCs  Passed  to \code{\link{goodERCCs}}
#' @param allowfailed, allowfailed.frac,mad.factor  Passed  to \code{\link{goodwells}}
#' @param min.good.empties   Passed to \code{\link{livecellcutoff}}
#' @return  A list of wellsets, named as above.
#'  For the benefit of platediagnostics QC, an additional list item \code{.info}
#' (note the leading dot) is returned. It contains details about the procedure, specifically:
#'  * livecellcutoff.orig original number returned by \code{\link{livecellcutoff}} (needed for QC)
#'  * livecellcutoff: cutoff used for the 'live' category; \code{max(livecellcutoff.orig, min_liveness_cutoff)}
#'  * gooderccs: identities of the 'good enough' ERCCs (needed for QC)
#'  * degraded: if TRUE, no ERCC's are were good enough so top-3 was used.
#' @examples \dontrun{
#'   alldata <- SCutils::read.counts(file) # reads a TMnnn.transcripts.txt file
#'   
#'   wellsets <- find.wellsets(data=alldata,
#'                             empties='col24',
#'                             ignore=NULL, # default anyway
#'                             species='human', # default anyway
#'                             badERCCs='fail') # default anyway
#'
#'
#'   # Show overview of all the wellsets that were defined:
#'   cbind(unlist(lapply(wellsets, length)))
#'   
#'   data <- alldata[ , wellsets$fullgood ]
#'   data <- data[ rowSums(data) > 0, ]
#'   ## at this point, data contains only non-empty wells that
#'   ## have not failed and should not be ignored 
#' }
#' 1
find.wellsets <- function(data,
                          empties,
                          ignore=NULL,
                          species='human',
                          badERCCs=c('fail','use.top3'),
                          min_liveness_cutoff=500,
                          good.ERCC.median=4,
                          min.req.ERCCs=2,
                          allowfailed=1,
                          allowfailed.frac=0.1,
                          mad.factor=3,
                          min.good.empties=2) {
  badERCCs <- match.arg(badERCCs)
  stopifnot(length(intersect(empties,ignore))==0)

  .info <- list() #for additional stuff like cutoff
  
  ws <- list(unk='UNK',
             allknown = wellname()[-1], # ignore first elt which is 'UNK'
             empty = lookup.wellset(empties),
             ignore=lookup.wellset(ignore))
  rm(empties, ignore) #forces the use of ws$STUFF
  ws$wanted <- with(ws,setdiff(allknown, ignore))
  ws$full <- with(ws, setdiff(wanted,empty))

  if ( species == 'yeast' ) {        #ugly but well
    mitos <- keepmito(data, regexp="^Q0")
    genes <- rmmito(rmspike(data),
                    regexp="^Q0")          #meaning: gene txpts (without mitochondrials)
  } else {
    mitos <- keepmito(data)
    genes <- rmmito(rmspike(data))          #meaning: gene txpts (without mitochondrials)
  }
  spikes <- keepspike(data)
  
  goodERCCs <- goodERCCs(spikes,
                         good.ERCC.median=good.ERCC.median,
                         min.req.ERCCs=min.req.ERCCs)
                         
  cat(nrow(goodERCCs), " good ERCCs\n")

  if( nrow(goodERCCs) ==0 ) {
    msg <- sprintf("find.wellsets: Could not find enough good ERCCs\n")
    warning(msg)
    if(badERCCs=='fail')
      stop("Exiting.")
    warning("Will continue in degraded mode using simply the top 3 ERCCs\n")
    .info$degraded.mode <- TRUE
    cat(file=stderr(), msg)
    tot <- apply(spikes, 1, sum, na.rm=TRUE)
    top3 <- order(tot, decreasing=TRUE)[1:3]
    goodERCCs <- spikes[ top3, ]
  } else { .info$degraded.mode <- FALSE }
  .info$gooderccs <- rownames(goodERCCs)

  ws$good <- goodwells(goodERCCs,
                       allowfailed=1,
                       allowfailed.frac=0.1,
                       mad.factor=3)
  if(length(ws$good)==0)
    stop("find.wellsets: No good wells found")
  
  ws$wantedgood <- with(ws, intersect(wanted,good))
  ws$ignoredgood <- with(ws, intersect(ignore,good)) 
  ws$failed <- with(ws, setdiff(allknown, good))
  ws$wantedfailed <- with(ws, intersect(wanted,failed))
  ws$ignoredfailed <- with(ws, intersect(ignore,failed))
  ws$emptyfailed <- with(ws, intersect(empty,wantedfailed))
  ws$emptygood <- with(ws, intersect(empty,wantedgood))
  ws$fullfailed <- with(ws, intersect(full,wantedfailed))
  ws$fullgood <- with(ws,intersect(full,wantedgood))
  
  cat(length(ws$wantedgood), " wanted good wells\n")
  cat(length(ws$emptyfailed), " wanted empties are bad\n")
  
  cutoff <- livecellcutoff(genes,
                           wellsets=ws,
                           mad.factor=mad.factor,
                           min.good.empties=min.good.empties) # only looks at wantedgood@@@
  .info$livecellcutoff.orig <- cutoff
  if (is.na(cutoff) || cutoff < min_liveness_cutoff) { 
    warning(sprintf("find.wellsets: liveness cutoff is %.1f. This is too low, using %.0f instead ***",
                    cutoff, min_liveness_cutoff))
    cutoff <- min_liveness_cutoff
  }
  .info$livecellcutoff <- cutoff

  ## at this point we should dump any failed and/or ignored wells, and
  ## any genes not expressed:
  data <- data[ rowSums(data) > 0,  ws$wantedgood]

  logTxpts <- log2(colSums(genes)+1)

  exceeding <- names(which(logTxpts>log2(cutoff)))
  if (length(exceeding) == 0)
    stop("find.wellsets: all cells dead?")
  
  ws$live <- with(ws, Reduce(intersect, list(wantedgood, full, exceeding)))
  ws$dead <- with(ws, setdiff(fullgood, live))
  
  ## double check stuff. Don't use setequal as it ignores duplicates.
  ## 'leaf nodes' of the 'allknown' hierarchies:
  stopifnot(with(ws, identical(sort(c(ignore, wanted)),
                               sort(c(good,failed)))))
  
  ## 'leaf nodes' of the 'wanted' hierarchies:
  stopifnot(with(ws, identical(sort(c(wantedgood,wantedfailed)),
                               sort(c(emptygood,emptyfailed,
                                      fullgood,fullfailed)))))
  ws$.info <- .info
  return(ws)
} # find.wellsets
