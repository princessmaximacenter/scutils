<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [TODO:](#markdown-header-todo)
- [1.24 (31-Jul-2024)](#markdown-header-124-31-jul-2024)
- [1.23 (23-Apr-2024)](#markdown-header-123-23-apr-2024)
- [1.122 (7-Mar-2024)](#markdown-header-1122-7-mar-2024)
- [1.121 (26-Jan-2024)](#markdown-header-1121-26-jan-2024)
- [1.121 (26-Jan-2024)](#markdown-header-1121-26-jan-2024_1)
- [1.120 (16-Feb-2023)](#markdown-header-1120-16-feb-2023)
- [1.119 (16-Feb-2023)](#markdown-header-1119-16-feb-2023)
- [1.118 (4-Jan-2023 )](#markdown-header-1118-4-jan-2023-)
- [1.117 (30-Dec-2022)](#markdown-header-1117-30-dec-2022)
- [1.116 (29-Dec-2022)](#markdown-header-1116-29-dec-2022)
- [1.115 (22-Dec-2022)](#markdown-header-1115-22-dec-2022)
- [1.114 (20-Dec-2022)](#markdown-header-1114-20-dec-2022)
- [1.113 (2-Dec-2022)](#markdown-header-1113-2-dec-2022)
- [1.112 (16-Nov-2022)](#markdown-header-1112-16-nov-2022)
- [1.111 (14-Nov-2022)](#markdown-header-1111-14-nov-2022)
- [1.110 (27-Oct-2022)](#markdown-header-1110-27-oct-2022)
- [1.109  (27-Sep-2022)](#markdown-header-1109-27-sep-2022)
- [1.108 (6-Sep-2022)](#markdown-header-1108-6-sep-2022)
- [1.107 (30-Aug-2022)](#markdown-header-1107-30-aug-2022)
- [1.106 (27-Jul-2022)](#markdown-header-1106-27-jul-2022)
- [1.105 (21-Jul-2022)](#markdown-header-1105-21-jul-2022)
- [1.104 (2-Jun-2022)](#markdown-header-1104-2-jun-2022)
- [1.103 (30-May-2022)](#markdown-header-1103-30-may-2022)
- [1.102 (27-May-2022)](#markdown-header-1102-27-may-2022)
- [1.101 (25-May-2022)](#markdown-header-1101-25-may-2022)
- [1.98](#markdown-header-198)
- [1.97](#markdown-header-197)
- [1.96](#markdown-header-196)
- [1.95](#markdown-header-195)
- [1.94](#markdown-header-194)
- [1.92](#markdown-header-192)
- [1.91](#markdown-header-191)
- [1.90](#markdown-header-190)
- [1.88](#markdown-header-188)
- [1.87](#markdown-header-187)
- [1.86](#markdown-header-186)
- [1.85](#markdown-header-185)
- [1.70](#markdown-header-170)
- [earlier stuff: see git log](#markdown-header-earlier-stuff-see-git-log)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# TODO: 
* add mitochondrial genes as a separate genelist (also to confounder_genelists)
* make lookup.genes.byGOid(discard.evidence=NULL) the default
* improve gene list versioning
* check/document wether metadataCorrelations uses the RNA or SCT assay 
* use a proper loop in genomedata.R

# 1.24 (31-Jul-2024)
* added new more robust derivedCellcycleGenes2 function, old derivedCellcycleGenes depracated
* no more theme_pubr

# 1.23 (23-Apr-2024)
* built-in genome data now compiled using R 4.3; this is incompatible with earlier R versions.
* `read10xlibs` now has keep_mito option
* docu + bug fixes

# 1.122 (7-Mar-2024)
* various bug fixes
* read10xlibs now takes assay.version
* added function auto_threshold_cnv
* assay.version for read10xlibs

# 1.121 (26-Jan-2024)
* added function auto_threshold_cnv
* fixed problem with wellname() and find.wellsets(), and adjust docu and example code
* added assay.version option to read10xlibs() to be able to use older objects

# 1.121 (26-Jan-2024)
* fixed problem with wellname() and find.wellsets(), and adjust docu and example code
* added assay.version option to read10xlibs() to be able to use older objects

# 1.120 (16-Feb-2023)
* now really fixed that error mesg :-/

# 1.119 (16-Feb-2023)
* fixed bug + error messages in read10xlibs

# 1.118 (4-Jan-2023 )
* added functions addCNVAssay, infercnv_{reference,observation}_cells

# 1.117 (30-Dec-2022)
* fixed alignment problem on derivedCellcycleGenes

# 1.116 (29-Dec-2022)
* revamped derivedCellcycleGenes

# 1.115 (22-Dec-2022)
* saneAddModuleScore can now handle empty genelists (ignoring or using substitute values)

# 1.114 (20-Dec-2022)

* added function aneuploidy_score
* improved documentation

# 1.113 (2-Dec-2022)
* inaccuracies in documentation

# 1.112 (16-Nov-2022)
* speed/memory optimizations for read10xlibs and fast_merge_sparse

# 1.111 (14-Nov-2022)

* correct data error: lookup.malegenes returned also Y-chromosome genes
  from the pseudoautosomal region. Both lookup.malegenes and the genelists
  data from data(GENOMEDATA) have been corrected.

# 1.110 (27-Oct-2022)
* human genome data was wrong (had mouse orthologues for the cellphase specific genes ...)
* read10xlibs now can also read per_sample_outs/.../count/sample_filtered_feature_bc_matrix type data.

# 1.109  (27-Sep-2022)
* adjusted docu. reformatted man pages on genomedata
* fast_merge_sparse now even faster
* default assay.type for metadataCorrelations now 'RNA'
* added function AddDimReduc

# 1.108 (6-Sep-2022)
* added function sortable_integer_strings

# 1.107 (30-Aug-2022)
* mito_pattern now matched case-insensitively.
* fixed documentation inaccuracies

# 1.106 (27-Jul-2022)

* renamed the genome data to match the ones we have on disk, i.e.
  `refdata_gex_GRCh38_2020_A`, `refdata_gex_mm10_2020_A` etc. Do ?genomedata for details
* All chrX and chrY genes were missing from the `genecoords` objects, they have
  been added
* A data object genomedata_overview has been added, documenting the genomedata objects
  that are available
* Misc. small fixes in code and in documentation

# 1.105 (21-Jul-2022)

* added genomedata, providing uniform access to coordinates and gene lists.
* For this, function coordinates_from_gtf has been added (although there is not
  much need to use it). get_genome_coords has been renamed to biomart_gene_coordinates

# 1.104 (2-Jun-2022)

 * read10xlibs now reads HDF5 files (saves 50% space). 
 * fixed bug in lookup.genes.byGOterm (also used by lookup.stressgenes
   and lookup.ribogenes) leading to way too few relevant genes being
   retrieved. Genelists data have also been fixed now.
 * New genelist chr6HLA genes added (so they be ignored in inferCNV)
 * fixed bug in unENSG

# 1.103 (30-May-2022)

* extensive docu, see README.md

# 1.102 (27-May-2022)

* added data human_10x.rda and mouse_10x.rda which makes available a
  genelists object containing various gene lists. See ?genelists for docu.

# 1.101 (25-May-2022)
* added functions DimRedToMetaData, bulkCNV2infercnv, gene_order2GRanges, fixed bugs
* added data genelists_human_10x and genelists_mouse_10x, containing lists of gene names for various biological processes

# 1.98

* Ixed bug if there are no mitochondria in object

# 1.97

* Added loadings.MAD.plot, AddLookedupMetadat, lookup.genes.byGOid, hasGOid and 
  get_gene_coordinates, + misc bug fixes.

# 1.96

* Added various functions from
  git/sc-facility/Rscripts/misc-functions.R, both 10X-specific and
  general, and including various general gene-lookup functions which
  should work for human and mouse using AnnotationDbi as the source.

# 1.95

* misleading documenation (no ignore argument in goodERCCs)

# 1.94

* added 10X-specific functions read10xlibs and prepare.10x.annotation

# 1.92

Extended lookup.wellsets() so it can parse well coordinates (see its examples)

# 1.91

* added find.wellsets(), which simplifies the find of good (and live)  wells enormously.

# 1.90

* changes to do with properly managing ignored wells, which are now
  implicit in whatever data columns there are.  The 'all' wellset has
  been replaced with 'allknown' and 'wanted'. The latter is the
  setdifference 'allknown - ignored' . goodERCCs() and goodwells()
  always refer to the whole plate, so lost their 'ignore'
  arguments. livecellcutoff() only looks at whatever cells are in the data
  matrix.
* read.counts() now takes arguments ignore.wells and keep.unk
* SCutils:::.gitversion() now works

# 1.88

* gitversion (previously getversion) now uses GIT_VERSION produced by pre-commit hook

# 1.87

* traceback.dump.quit replaced with dumper

# 1.86

* SCutils now merged with SCutils2 (which is not needed anymore)
* It can now be installed using devtools::install_bitbucket("princessmaximacenter/scutils")

# 1.85 

* split off from Sharq/SCutils for easier installing and
  maintenance. Now has its own repository.

* added {de,}commafy, format.kMG, getversion, read.json, santitize,
  {write,write}.tab, traceback.dump.quit functions from uuutilss to cut down on
  number of packages to be installed

# 1.70

* minimum liveness cutoff of 500 (and new livecellcutoff argument)

# earlier stuff: see git log
  
  

